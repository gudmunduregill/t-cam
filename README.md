##Project Goal
The goal of the project was to ascertain whether it is feasible to use computer vision to track a food product through its process. The module should be able to communicate with Marel's automation system and receive information about a piece and signal back when a piece has reach a predefined position in the process.


## How is this project setup?

Our project's target machine runs Ubuntu 12.04.

1. Project is built on various development machines.
2. Marel's [https://students.mymarel.com/jenkins/]  listens to changes, builds and tests.

## Branches

Our development is done on varius branches wich name is descriptive, pullin into 'development' is only allowed after pulling and ensuring a compatable patch



