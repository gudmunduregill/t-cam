angular.module('GuiApp').controller('BaseView', ['$scope', '$document', 'PlutoVariables', 'PlutoWebSocket', 'widgetService', 'PlutoREST', function($scope, $document, PlutoVariables, PlutoWebSocket, widgetService, PlutoREST){

    // // // // // // // // // // // // // // // // //
    //          Initialization of Variables
    // // // // // // // // // // // // // // // // //

    VIDEO_SIZE = 0.85; // 1.0 fills window

    var ctrl = this;
    var coordScaling = 1;
    var maskCanvas = null;
    var maskContext = null;
    var containerWidth = 0;
    var containerHeight = 0;
    var wrh = 1;
    var newWidth = 5;
    var newHeight = 5;
    var eventsAttached = false;
    var IP = 'localhost:8183';
    var dataLoaded = false;
    //var sendData = {};
    var pws = new PlutoWebSocket(IP);    

    var videoLength;
    var video = document.getElementById('videoPlayer');
    var source = document.createElement('source'); 
    var videoSwap = 0;

    var flowA = false;
    var flowB = false;
    var flowC = false;
    var flowD = false;
    var areaOfInterest = new flowBox(x0, y0, x1, y1);
    var flowBoxWidth = 10;
    var flowBoxA = new flowBox(x0 - flowBoxWidth, y0, x0 + flowBoxWidth, y1);
    var flowBoxB = new flowBox(x0, y0 - flowBoxWidth, x1, y0 + flowBoxWidth);
    var flowBoxC = new flowBox(x1 - flowBoxWidth, y0, x1 + flowBoxWidth, y1);
    var flowBoxD = new flowBox(x0, y1 - flowBoxWidth, x1, y1 + flowBoxWidth);
    var x0 = null; 
    var y0 = null;
    var x1 = null;
    var y1 = null;
    var A = "Flow left to right";
    var B = "Flow top to bottom";
    var C = "Flow right to left";
    var D = "Flow bottom to top";
    ctrl.videoFiles = [];
    ctrl.selectedVideo = "";
    $scope.selectedVideo; 
    $scope.videoFiles = [];
    clientPoI = [];

    // Booleans for which image to display
    videoFeed = {rawFeed: 'true',
		 tcamFeed: 'false',
		 hsvFeed: 'false'};

    var rawImage = new Image();
    var tcamImage = new Image();
    var hsvImage = new Image();

    ctrl.videoCanvas = null;
    ctrl.maskCanvas = null;
    ctrl.context = null;
    currentImageRatio = null;
    clientAreaOfInterest = null;
    serverAreaOfInterest = null;
    //currentImageWidth = null;
    //currentImageHeight = null;
    ctrl.flowDirection;
    ctrl.flowAoI = null;
    currentImage = new Image();

    serverPoI = [];
    //clientPoI = [];
    var selectedPoint;
    var pointNumber = 0;
    xScaling = null;
    yScaling = null;

    // // // // // // // // // // // // // // // // //
    // Pluto Pin Subscriptions And Server Connection
    // // // // // // // // // // // // // // // // //

    var subscription_handle_tCamIP = null;

    // Subscribe to adressIP
    subscription_handle_tCamIP = PlutoVariables.subscribe('string.wsserver.0.tCamIP' , function(tCamIP) {
                    ctrl.tCamIP = tCamIP;
     });

    //Pluto pin  subscriptions
    var subscription_handle_hMin = null;

    //Subscribe to hMin
    subscription_handle_hMin = PlutoVariables.subscribe('integer.wsserver.0.hMin' , function(hMin) {
                    ctrl.hMin = hMin;
    }); 

    //Subscribe to hMin
    subscription_handle_isRecording = PlutoVariables.subscribe('DIO.wsserver.0.isRecording' , function(hMin) {
                    ctrl.hMin = hMin;
    }); 
    pws.connect();

    pws.onConnect(function() {
	// Area Of Interest Read in From Config
        PlutoREST.readConfig("t-cam.0").then(function(config){
	    initVariables(config);
	});
    });

    widgetService.initialize().then(function () {
        PlutoREST.readConfig("POI.0").then(function(config){
	    //numOfClientPoints = config["nPOI"];
	    ctrl.pointNumber = config["nPOI"];
	    serverPoI = [];
                 
            //for (var i = 0; i < numOfClientPoints; i++){
            for (var i = 0; i < ctrl.pointNumber; i++){
                serverPoI.push(new POI(i, config["POIx" + i], config["POIy" + i]));
            }
            selectedPoint = 0;
        });
        updateVideoList();
        wsInitialized = true;
    });


    ctrl.routerOnHide = function() { // Unsubscribe
        PlutoVariables.unsubscribe(subscription_handle_tCamIP);
	PlutoVariables.unsubscribe(subscription_handle_hMin);          
    }

    // // // // // // // // // // // // // // // // //
    //         Events Attaching And Firing
    // // // // // // // // // // // // // // // // //

    $(window).bind('resize', resizeCanvasToWindow); // Run canvasReSize function on window resize

    pws.onMessage(function(msg) {


        var jdata = angular.fromJson(msg);      
        if (jdata.liveData != null) { // Raw Video Feed
            rawImage.src = "data:image/jpeg;base64," + jdata.liveData;       
        }
	if (jdata.productionData != null) { // TCAM Video Feed 
            tcamImage.src = "data:image/jpeg;base64," + jdata.productionData;
	}
	if (jdata.contrastData != null){ // HSV Video Feed
	    hsvImage.src = "data:image/jpeg;base64," + jdata.contrastData;
	}
	// Set image by boolean, decided in UI selection, raw feed by default
	if (videoFeed.rawFeed) { 
	    currentImage = rawImage;
	    if (rawImage.width != 0) {
		displayImage(rawImage);
	    }
	} else if (videoFeed.tcamFeed) {
	    currentImage = tcamImage;
	    if (tcamImage.width != 0) {
		displayImage(tcamImage);
	    }
	} else if (videoFeed.hsvFeed) {
	    currentImage = hsvImage;
	    if (hsvImage.width != 0) {
		// HSV Image needs a special display function because it has different dimensions
		displayHsvImage(hsvImage); 
	    }
	} else if (videoFeed.recordSource) {
	    if (rawImage.width != 0) {
		displayRecordSourceImage(tcamImage);
	    }
	}
    });

    // // // // // // // // // // // // // // // // //
    //                 Prototypes
    // // // // // // // // // // // // // // // // //

    function POI(id,x,y) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.moving = false;
    }

    function drawFlowAoI(img) {
	var maskContext = ctrl.maskCanvas.getContext('2d');
        var xScaling = img.width / ctrl.maskCanvas.width;
        var yScaling = img.height / ctrl.maskCanvas.height;

        ctrl.flowAoI = new flowBox(x0 / xScaling, y0 / yScaling, x1 / xScaling, y1 / yScaling);

	flowBoxA = new flowBox(ctrl.flowAoI.x0 - flowBoxWidth,
			       ctrl.flowAoI.y0,
			       ctrl.flowAoI.x0 + flowBoxWidth,
			       ctrl.flowAoI.y1);
	flowBoxB = new flowBox(ctrl.flowAoI.x0,
			       ctrl.flowAoI.y0 - flowBoxWidth,
			       ctrl.flowAoI.x1, ctrl.flowAoI.y0 + flowBoxWidth);
	flowBoxC = new flowBox(ctrl.flowAoI.x1 - flowBoxWidth,
			       ctrl.flowAoI.y0,
			       ctrl.flowAoI.x1 + flowBoxWidth,
			       ctrl.flowAoI.y1);
	flowBoxD = new flowBox(ctrl.flowAoI.x0,
			       ctrl.flowAoI.y1 - flowBoxWidth,
			       ctrl.flowAoI.x1,
			       ctrl.flowAoI.y1 + flowBoxWidth);

        maskContext.lineWidth = 5;
        maskContext.strokeStyle = 'orange';

        maskContext.strokeRect(ctrl.flowAoI.x0,
			       ctrl.flowAoI.y0,
			       ctrl.flowAoI.x1 - ctrl.flowAoI.x0,
			       ctrl.flowAoI.y1 - ctrl.flowAoI.y0);
	drawFlowBoxes();
    }

    function drawFlowBoxes() {
	if (flowA == true) {
	    drawFlowBox(ctrl.flowAoI.x0, ctrl.flowAoI.y0, ctrl.flowAoI.x0, ctrl.flowAoI.y1, "green");
	    drawFlowBox(ctrl.flowAoI.x1, ctrl.flowAoI.y0, ctrl.flowAoI.x1, ctrl.flowAoI.y1, "red");
	} else if(flowB == true) {
	    drawFlowBox(ctrl.flowAoI.x0, ctrl.flowAoI.y0, ctrl.flowAoI.x1, ctrl.flowAoI.y0, "green");
	    drawFlowBox(ctrl.flowAoI.x0, ctrl.flowAoI.y1, ctrl.flowAoI.x1, ctrl.flowAoI.y1, "red");
	} else if(flowC == true) {
	    drawFlowBox(ctrl.flowAoI.x1, ctrl.flowAoI.y0, ctrl.flowAoI.x1, ctrl.flowAoI.y1, "green");
	    drawFlowBox(ctrl.flowAoI.x0, ctrl.flowAoI.y0, ctrl.flowAoI.x0, ctrl.flowAoI.y1, "red");
	} else if(flowD == true){	
	    drawFlowBox(ctrl.flowAoI.x0, ctrl.flowAoI.y1, ctrl.flowAoI.x1, ctrl.flowAoI.y1, "green");
	    drawFlowBox(ctrl.flowAoI.x0, ctrl.flowAoI.y0, ctrl.flowAoI.x1, ctrl.flowAoI.y0, "red");
	}
    }
    
    function drawFlowBox(coord0, coord1, coord2, coord3, colour) {
	var maskContext = ctrl.maskCanvas.getContext('2d');
	maskContext.beginPath();
	maskContext.moveTo(coord0, coord1);
	maskContext.lineTo(coord2, coord3);
	maskContext.lineWidth = 10;
	maskContext.strokeStyle = colour;
	maskContext.stroke();
    }

    
    function flowBox(x0, y0, x1, y1) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
    }    

    function Box(x0, y0, width, height) {
	this.x0 = x0;
	this.y0 = y0;
	this.x1 = x0 + width;
	this.y1 = y0 + height;
    }

    function AreaOfInterest(x0, y0, width, height) {
	this.grabSize = 20; // Size of area around the edges that client can grab.
	this.x0 = x0;
	this.y0 = y0;
	this.width = width;
	this.height = height;
	this.x1 = this.x0 + width;
	this.y1 = this.y0 + height;
	this.outsideTopLeft = new Box(this.x0 - this.grabSize, this.y0 - this.grabSize, this.grabSize, this.grabSize);
	this.outsideTop = new Box(this.x0, this.y0 - this.grabSize, this.width, this.grabSize);
	this.outsideTopRight = new Box(this.x1, this.y0 - this.grabSize, this.grabSize, this.grabSize);
	this.insideTop = new Box(this.x0 + this.grabSize, this.y0, this.width - (2 * this.grabSize), this.grabSize);
	this.insideTopLeft = new Box(this.x0, this.y0, this.grabSize, this.grabSize);
	this.insideTopRight = new Box(this.x1 - this.grabSize, this.y0, this.grabSize, this.grabSize);
	this.outsideBottom = new Box(this.x0, this.y1, this.width, this.grabSize);
	this.outsideBottomRight = new Box(this.x1, this.y1, this.grabSize, this.grabSize);
	this.outsideBottomLeft = new Box(this.x0 - this.grabSize, this.y1, this.grabSize, this.grabSize);
	this.insideBottom = new Box(this.x0 + this.grabSize, this.y1 - this.grabSize, this.width - 2 * this.grabSize, this.grabSize);
	this.insideBottomLeft = new Box(this.x0, this.y1 - this.grabSize, this.grabSize, this.grabSize);
	this.insideBottomRight = new Box(this.x1 - this.grabSize, this.y1 - this.grabSize, 2 * this.grabSize, 2 * this.grabSize);
	this.outsideRight = new Box(this.x1, this.y0, this.grabSize, this.height);
	this.outsideLeft = new Box(this.x0 - this.grabSize, this.y0, this.grabSize, this.height);
	this.insideRight = new Box(this.x1 - this.grabSize, this.y0 + this.grabSize, this.grabSize, this.height - (2 * this.grabSize));
	this.insideLeft = new Box(this.x0, this.y0, this.grabSize, this.height - this.grabSize);
	this.insideBox = new Box(this.x0 + this.grabSize, this.y0 + this.grabSize, this.width - (2*this.grabSize), this.height - (2*this.grabSize));
    }

    // // // // // // // // // // // // // // // // //
    //                 Functions
    // // // // // // // // // // // // // // // // //

    //Loads when adjust gates is
    //selected used to update POI values
    //on mouse move
    function eventMouseMovePoI(e) {
	for (var i = 0; i < clientPoI.length; i++) {
	    if (clientPoI[i].moving) {
		updateP(i,(e.clientX-rect.left),(e.clientY-rect.top));
		drawPoI(clientPoI);
		if (wsInitialized && widgetService.InputSpinner.switchPoints.getValue() === i) {
		    widgetService.InputSpinner.xValue.setValue(clientPoI[i].x); 
		    widgetService.InputSpinner.yValue.setValue(clientPoI[i].y);
		}
	    }
	}
    }

    //Loads when adjust gates is
    //selected used to update POI values
    //on mouse up
    function eventMouseUpPoI(e) {
	for (var i = clientPoI.length-1; i >= 0; i--) {
	    if(clientPoI[i].moving){
		clientPoI[i].moving = false;
	    }
	}
    }
	
    
    //Loads when adjust gates is
    //selected used to update POI values
    //on mouse drag
    function eventMouseDownPoI(e){
	rect = mask.getBoundingClientRect();
	var mx = e.clientX - rect.left;
	var my = e.clientY - rect.top;
	for (var i = 0; i < ctrl.pointNumber; i++) {
	    if(isClicked(clientPoI[i], mx, my)){
		clientPoI[i].moving = true;
	    }
	}
    };
    
    function isAreaWithinBounds(x, y, width, height) {
	return (x >= 0
		&& y >= 0
		&& x + width <= ctrl.videoCanvas.width
		&& y + height <= ctrl.videoCanvas.height
		&& width >= clientAreaOfInterest.grabSize 
		&& height >= clientAreaOfInterest.grabSize)
    }
	
    function initializeFlowBoxes() {
	ctrl.flowAoI = new flowBox(x0, y0, x1, y1);
	boxWidth = 10;
	boxA = new flowBox(x0 - flowBoxWidth, y0, x0 + flowBoxWidth, y1);
	boxB = new flowBox(x0, y0 - flowBoxWidth, x1, y0 + flowBoxWidth);
	boxC = new flowBox(x1 - flowBoxWidth, y0, x1 + flowBoxWidth, y1);
	boxD = new flowBox(x0, y1 - flowBoxWidth, x1, y1 + flowBoxWidth);

	var canvasObject = $('#mask');
	canvasObject.mousedown ( function(e) {
	    console.log('hello I am the greatest mousedown event');
	    if (ctrl.flowAoI !== null) {
		ctrl.maskCanvas.getContext('2d').clearRect(0, 0, ctrl.maskCanvas.width, ctrl.maskCanvas.height);
		var boundsOfElement = document.getElementById('mask').getBoundingClientRect(); 
		//Get mouse coordinates
		var mouseX = e.clientX - boundsOfElement.left;
		var mouseY = e.clientY - boundsOfElement.top;
		if (isInsideFlowBox(mouseX, mouseY,flowBoxA)) {
		    // Keep track of where in the object we clicked
		    // and close to what line of the AreaOfInterestflowBox
		    setFlowDirection(A, false)
		    drawFlowAoI(currentImage);
		    return;
		}
		else if(isInsideFlowBox(mouseX, mouseY,flowBoxB)) {
		    setFlowDirection(B, false)
		    drawFlowAoI(currentImage);
		    return;
		}
		else if(isInsideFlowBox(mouseX, mouseY,flowBoxC)) {
		    setFlowDirection(C, false)
		    drawFlowAoI(currentImage);
		    return;
		}           
		else if(isInsideFlowBox(mouseX, mouseY,flowBoxD)) {
		    setFlowDirection(D, false)
		    drawFlowAoI(currentImage);
		    return;
		} else {
		    drawFlowAoI(currentImage);
		    return;
		}
	    }
	});

    }

    function attachEventsToClientAreaOfInterest() {
	var imageWidth = currentImage.width;
	var imageHeight = currentImage.height;
	// To get the x and y offsets for the canvas element
	var boundsOfElement = document.getElementById('mask').getBoundingClientRect(); 
	var canvasObject = $('#mask');
	var isDragging = false;
	var hasStartedResizingToLeft = false;
	var startMouseX = 0;
	var startMouseY = 0;

	canvasObject.unbind();

	canvasObject.mousedown ( function(e) {
	    isDragging = true;
	    startMouseX = e.clientX - boundsOfElement.left;
	    startMouseY = e.clientY - boundsOfElement.top;
	    hasStartedResizingToLeft = false;
	});

	canvasObject.mouseup( function(e) { 
	    isDragging = false;
	    hasStartedResizingToLeft = false;

	    if (clientAreaOfInterest !== null) {
		// Send new Area Of Interest
		var newServerAreaOfInterest = serverAoiFromClientAoi(clientAreaOfInterest, imageWidth, imageHeight);
		//Set configuration storage
                var sendData = {};
		sendData['AOIx0'] = newServerAreaOfInterest.x0;
		sendData['AOIy0'] = newServerAreaOfInterest.y0;
		sendData['AOIx1'] = newServerAreaOfInterest.x1;
		sendData['AOIy1'] = newServerAreaOfInterest.y1;
		delete serverAreaOfInterest;
		serverAreaOfInterest = newServerAreaOfInterest;
		
		PlutoREST.setConfig("t-cam.0",sendData).then(function(){
		    updateInputSpinnersAOI(serverAreaOfInterest);
		    console.log('Config saved successfully');
		});
	    } else {
		console.log('Error. Some of the changes may not have been saved.')
	    }
	});

	canvasObject.mousemove( function(e) {
	    if (clientAreaOfInterest !== null) {
		var mouseX = e.clientX - boundsOfElement.left;
		var mouseY = e.clientY - boundsOfElement.top;
		if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideBox)) {
		    $(canvasObject).css( 'cursor', 'move' );
		    if (isDragging && !hasStartedResizingToLeft) {
			var deltaX = mouseX - startMouseX;
			startMouseX = mouseX;
			var deltaY = mouseY - startMouseY;
			startMouseY = mouseY;
			if (isAreaWithinBounds(clientAreaOfInterest.x0 + deltaX,
					     clientAreaOfInterest.y0 + deltaY,
					     clientAreaOfInterest.width,
					     clientAreaOfInterest.height)) {
			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0 + deltaX,
								    clientAreaOfInterest.y0 + deltaY,
								    clientAreaOfInterest.width,
								    clientAreaOfInterest.height);
			    delete clientAreaOfInterest;
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.outsideTopLeft)) {
		    $(canvasObject).css( 'cursor', 'nw-resize' );
		    if (isDragging) {
			var delta = clientAreaOfInterest.x0 - mouseX;
			if (isAreaWithinBounds(mouseX, mouseY, (clientAreaOfInterest.width + delta), (clientAreaOfInterest.height + delta))) {
			    var newAreaOfInterest = new AreaOfInterest(mouseX, mouseY,
								    clientAreaOfInterest.width + delta,
								    clientAreaOfInterest.height + delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.outsideTop)) {
		    $(canvasObject).css( 'cursor', 'n-resize' );
		    if (isDragging) {
			var containerHeight = clientAreaOfInterest.height + (clientAreaOfInterest.y0 - mouseY);
			if (isAreaWithinBounds(clientAreaOfInterest.x0, mouseY, clientAreaOfInterest.width, containerHeight)) {
			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0,
								    mouseY,
								    clientAreaOfInterest.width,
								    containerHeight);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.outsideTopRight)) {
		    $(canvasObject).css( 'cursor', 'ne-resize' );
		    if (isDragging) {
			var delta = mouseX - clientAreaOfInterest.x1;
			if (isAreaWithinBounds(clientAreaOfInterest.x0, mouseY, clientAreaOfInterest.width + delta, clientAreaOfInterest.height + delta)) {
			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0, mouseY,
								    clientAreaOfInterest.width + delta,
								    clientAreaOfInterest.height + delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideTop)) {
		    $(canvasObject).css( 'cursor', 's-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y0;
			if (isAreaWithinBounds(clientAreaOfInterest.x0, mouseY,
								clientAreaOfInterest.width,
					     clientAreaOfInterest.height - delta)) {
			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0, mouseY,
								    clientAreaOfInterest.width,
								    clientAreaOfInterest.height - delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideTopLeft)) {
		    $(canvasObject).css( 'cursor', 'se-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y0;
			if (isAreaWithinBounds(mouseX, mouseY,
								clientAreaOfInterest.width - delta,
					     clientAreaOfInterest.height - delta)) {

			    var newAreaOfInterest = new AreaOfInterest(mouseX, mouseY,
								    clientAreaOfInterest.width - delta,
								    clientAreaOfInterest.height - delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			    }
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideTopRight)) {
		    $(canvasObject).css( 'cursor', 'sw-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y0;
			if (isAreaWithinBounds(clientAreaOfInterest.x0, mouseY,
								clientAreaOfInterest.width - delta,
					     clientAreaOfInterest.height - delta)) {

			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0, mouseY,
								    clientAreaOfInterest.width - delta,
								    clientAreaOfInterest.height - delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			    }
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.outsideBottom)) {
		    $(canvasObject).css( 'cursor', 's-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y1;
			if (isAreaWithinBounds(clientAreaOfInterest.x0,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width,
					     clientAreaOfInterest.height + delta)) {

			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width,
								    clientAreaOfInterest.height + delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.outsideBottomRight)) {
		    $(canvasObject).css( 'cursor', 'se-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y1;
			if (isAreaWithinBounds(clientAreaOfInterest.x0,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width + delta,
					     clientAreaOfInterest.height + delta)) {

			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width + delta,
								    clientAreaOfInterest.height + delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.outsideBottomLeft)) {
		    $(canvasObject).css( 'cursor', 'sw-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y1;
			if (isAreaWithinBounds(mouseX,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width + delta,
					     clientAreaOfInterest.height + delta)) {

			    var newAreaOfInterest = new AreaOfInterest(mouseX,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width + delta,
								    clientAreaOfInterest.height + delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideBottom)) {
		    $(canvasObject).css( 'cursor', 'n-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y1;
			if (isAreaWithinBounds(clientAreaOfInterest.x0,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width,
					     clientAreaOfInterest.height + delta)) {

			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width,
								    clientAreaOfInterest.height + delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideBottomLeft)) {
		    $(canvasObject).css( 'cursor', 'ne-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y1;
			if (isAreaWithinBounds(mouseX,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width + delta,
					     clientAreaOfInterest.height + delta)) {

			    var newAreaOfInterest = new AreaOfInterest(mouseX,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width + delta,
								    clientAreaOfInterest.height + delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideBottomRight)) {
		    $(canvasObject).css( 'cursor', 'nw-resize' );
		    if (isDragging) {
			var delta = mouseY - clientAreaOfInterest.y1;
			if (isAreaWithinBounds(clientAreaOfInterest.x0,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width + delta,
					     clientAreaOfInterest.height + delta)) {

			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width + delta,
								    clientAreaOfInterest.height + delta);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.outsideRight)) {
		    $(canvasObject).css( 'cursor', 'e-resize' );
		    if (isDragging) {
			var delta = mouseX - clientAreaOfInterest.x1;
			if (isAreaWithinBounds(clientAreaOfInterest.x0,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width + delta,
					     clientAreaOfInterest.height)) {

			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width + delta,
								    clientAreaOfInterest.height);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.outsideLeft)) {
		    $(canvasObject).css( 'cursor', 'w-resize' );
		    if (isDragging) {
			var delta = clientAreaOfInterest.x0 - mouseX;
			if (isAreaWithinBounds(mouseX,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width + delta,
					     clientAreaOfInterest.height)) {

			    var newAreaOfInterest = new AreaOfInterest(mouseX,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width + delta,
								    clientAreaOfInterest.height);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideRight)) {
		    $(canvasObject).css( 'cursor', 'w-resize' );
		    if (isDragging) {
			var delta = clientAreaOfInterest.x1 - mouseX;
			if (isAreaWithinBounds(clientAreaOfInterest.x0,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width - delta,
					     clientAreaOfInterest.height)) {

			    var newAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width - delta,
								    clientAreaOfInterest.height);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
		    }
		} else if (isInsideBox(mouseX, mouseY, clientAreaOfInterest.insideLeft)) {
		    $(canvasObject).css( 'cursor', 'e-resize' );
		    if (isDragging) {
			var delta = mouseX - clientAreaOfInterest.x0;
			if (isAreaWithinBounds(mouseX,
								clientAreaOfInterest.y0,
								clientAreaOfInterest.width - delta,
					     clientAreaOfInterest.height)) {

			    var newAreaOfInterest = new AreaOfInterest(mouseX,
								    clientAreaOfInterest.y0,
								    clientAreaOfInterest.width - delta,
								    clientAreaOfInterest.height);
			    delete clientAreaOfInterest
			    clientAreaOfInterest = newAreaOfInterest;
			    drawAreaOfInterest(clientAreaOfInterest);
			}
	    }
		} else {
		    $(canvasObject).css( 'cursor', 'default' );
		}
	    }
	});
    }

    function updateInputSpinnersAOI(serverAreaOfInterest) {
	// X Value
	widgetService.InputSpinner.aoiXchange.setValue(serverAreaOfInterest.x0);
	widgetService.InputSpinner.aoiXchange.setMinValue(0);
	widgetService.InputSpinner.aoiXchange.setMaxValue(currentImage.width - (serverAreaOfInterest.x1 - serverAreaOfInterest.x0));

	// Y Value
	widgetService.InputSpinner.aoiYchange.setValue(serverAreaOfInterest.y0);
	widgetService.InputSpinner.aoiYchange.setMinValue(0);
	widgetService.InputSpinner.aoiYchange.setMaxValue(currentImage.height - (serverAreaOfInterest.y1 - serverAreaOfInterest.y0));

	console.log('serverAOI.y0 is ' + serverAreaOfInterest.y0);

	// Width
	widgetService.InputSpinner.aoiWidthChange.setValue(serverAreaOfInterest.x1 - serverAreaOfInterest.x0);
	widgetService.InputSpinner.aoiWidthChange.setMaxValue(currentImage.width - serverAreaOfInterest.x0);
	widgetService.InputSpinner.aoiWidthChange.setMinValue(1);

	// Height
	widgetService.InputSpinner.aoiHeightChange.setValue(serverAreaOfInterest.y1 - serverAreaOfInterest.y0);
	widgetService.InputSpinner.aoiHeightChange.setMaxValue(currentImage.height - serverAreaOfInterest.y0);
	widgetService.InputSpinner.aoiHeightChange.setMinValue(1);
    }
    
    function initClientAreaOfInterest(serverAreaOfInterest,
				      imageWidth,
				      imageHeight) {

	updateInputSpinnersAOI(serverAreaOfInterest);

        xScaling = imageWidth / ctrl.maskCanvas.width;
        yScaling = imageHeight / ctrl.maskCanvas.height;

	var x0 = serverAreaOfInterest.x0 / yScaling;
	var y0 = serverAreaOfInterest.y0 / xScaling;

	var width = serverAreaOfInterest.width / yScaling;
	var height = serverAreaOfInterest.height / xScaling;

	clientAreaOfInterest = new AreaOfInterest(x0, y0, width, height);

	attachEventsToClientAreaOfInterest();
	drawAreaOfInterest(clientAreaOfInterest);
    }

    // Options:
    // "videoFeed.tcamFeed"
    // "videoFeed.rawFeed"
    // "videoFeed.hsvFeed"
    function setImageFeed(feed) {
	if (feed === 'tcamFeed') {
	    videoFeed.tcamFeed = true;
	    videoFeed.rawFeed = false;
	    videoFeed.hsvFeed = false;
	    videoFeed.recordSource = false;
	    currentImage = tcamImage;
	} else if (feed === 'rawFeed') {
	    videoFeed.tcamFeed = false;
	    videoFeed.rawFeed = true;
	    videoFeed.hsvFeed = false;
	    videoFeed.recordSource = false;
	    currentImage = rawImage;
	} else if (feed === 'hsvFeed') {
	    videoFeed.tcamFeed = false;
	    videoFeed.rawFeed = false;
	    videoFeed.hsvFeed = true;
	    videoFeed.recordSource = false;
	    currentImage = hsvImage;
	} else if (feed === 'recordSource') {
	    videoFeed.tcamFeed = false;
	    videoFeed.rawFeed = false;
	    videoFeed.hsvFeed = false;
	    videoFeed.recordSource = true;
	    currentImage = tcamImage;
	}
    }


    //Loads when adjust gates is 
    //selected used to update POI values
    //on mouse drag
    function eventMouseDownPoI(e){
        rect = mask.getBoundingClientRect();
        var mx = e.clientX - rect.left;
        var my = e.clientY - rect.top;
        for (var i = 0; i < ctrl.pointNumber; i++) {
            if(isClicked(clientPoI[i], mx, my)){
                clientPoI[i].moving = true;
            }
        }
    };


    
    function initializePoI(config) {
	setImageFeed('tcamFeed');

        ctrl.pointNumber = config["nPOI"];


        var xScaling = ctrl.maskCanvas.width / currentImage.width;
        var yScaling = ctrl.maskCanvas.height / currentImage.height;

	clientPoI = [];
	//for (var i = 0; i < serverPoI.length; i++) {
	for (var i = 0; i < ctrl.pointNumber; i++) {
	    clientPoI.push(new POI(i, serverPoI[i].x * xScaling, serverPoI[i].y * yScaling));
	    //console.log(clientPoI[i]);
	    drawPoI(clientPoI);
	}
	ctrl.maskCanvas.addEventListener('mousedown', eventMouseDownPoI);

	ctrl.maskCanvas.addEventListener('mousemove', eventMouseMovePoI);

        ctrl.maskCanvas.addEventListener('mouseup', eventMouseUpPoI);

	widgetService.InputSpinner.pointNumber.setValue(ctrl.pointNumber);
	//widgetService.InputSpinner.pointNumber.setValue(clientPoI.length);
	widgetService.InputSpinner.pointNumber.setMinValue(1);
	widgetService.InputSpinner.pointNumber.setMaxValue(12);
	widgetService.InputSpinner.pointNumber.setHelp("Give clientPoI, 1 to 12");

	widgetService.InputSpinner.switchPoints.setValue(0);
	widgetService.InputSpinner.switchPoints.setMinValue(0);
	widgetService.InputSpinner.switchPoints.setMaxValue(ctrl.pointNumber-1);
	widgetService.InputSpinner.switchPoints.setHelp("Give clientPoI, 1 to 12");

	widgetService.InputSpinner.xValue.setValue(clientPoI[0].x);
	widgetService.InputSpinner.xValue.setMinValue(0);
	widgetService.InputSpinner.xValue.setMaxValue(7000);
	widgetService.InputSpinner.xValue.setHelp("Change X value");

	widgetService.InputSpinner.yValue.setValue(clientPoI[0].y);
	widgetService.InputSpinner.yValue.setMinValue(0);
	widgetService.InputSpinner.yValue.setMaxValue(7000);
	widgetService.InputSpinner.yValue.setHelp("Change Y value");

    }
    
    function resizeCanvasToWindow() {
	var oldHeight = ctrl.maskCanvas.height;
	var oldWidth = ctrl.maskCanvas.width;

	adjustCanvas(ctrl.videoCanvas);
	adjustCanvas(ctrl.maskCanvas);

	var xDelta = ctrl.maskCanvas.width / oldWidth;
	var yDelta = ctrl.maskCanvas.height / oldHeight;

	var newClientAreaOfInterest = new AreaOfInterest(clientAreaOfInterest.x0 * xDelta,
							 clientAreaOfInterest.y0 * yDelta,
							 clientAreaOfInterest.width * xDelta,
							 clientAreaOfInterest.height * yDelta);
	delete clientAreaOfInterest;
	clientAreaOfInterest = newClientAreaOfInterest;
	attachEventsToClientAreaOfInterest();
	drawAreaOfInterest(clientAreaOfInterest);
    }

    function adjustCanvas(canvas) {
	var currentImageRatio = currentImage.width / currentImage.height;
	canvas.width = $('#tcambody').width() * VIDEO_SIZE;
	canvas.height = canvas.width / currentImageRatio;
	if (canvas.height > $('#tcambody').height() * VIDEO_SIZE) {
	    canvas.height = $('#tcambody').height() * VIDEO_SIZE - $('.inputTools').height();
	    canvas.width = canvas.height * currentImageRatio;
	}
    }

    function initializeCanvas(img) {
	// Set canvas and context for first time
	ctrl.videoCanvas = document.getElementById('baseview');
	ctrl.context = ctrl.videoCanvas.getContext('2d');

    	// Set imageRatio for first time
    	currentImageRatio = (img.width / img.height);

	adjustCanvas(ctrl.videoCanvas);

	xScaling = img.width / ctrl.videoCanvas.width;
	yScaling = img.height / ctrl.videoCanvas.height;
    }

    function initializeMask(img) {
	// Set canvas and context for first time
	ctrl.maskCanvas = document.getElementById('mask');
	ctrl.context = ctrl.videoCanvas.getContext('2d');

    	// Set imageRatio for first time
    	currentImageRatio = (img.width / img.height);
	adjustCanvas(ctrl.maskCanvas);
    }

    function displayHsvImage(image) {
    	currentImageRatio = (image.width / image.height);
	hsvCanvas = document.getElementById('adjustContrast');
	var context = hsvCanvas.getContext('2d');

	hsvCanvas.width = $(window).width() * VIDEO_SIZE;
	hsvCanvas.height = hsvCanvas.width / currentImageRatio;
        context.drawImage(image, 0, 0, hsvCanvas.width, hsvCanvas.height);
        img = "";
    }

    function displayImage(img){
	if (ctrl.videoCanvas === null ) {
	    initializeCanvas(img);
	    initializeMask(img);
        }
        ctrl.context.drawImage(img, 0, 0, ctrl.videoCanvas.width, ctrl.videoCanvas.height);
        img = "";
    }

    function displayRecordSourceImage(image) {
	// Set canvas and context for first time
	var canvas = document.getElementById('recordSource');
	context = canvas.getContext('2d');

    	// Set imageRatio for first time
    	currentImageRatio = (image.width / image.height);

	adjustCanvas(canvas);

	xScaling = image.width / canvas.width;
	yScaling = image.height / canvas.height;
	context.drawImage(image, 0, 0, canvas.width, canvas.height);
    }

    function drawAreaOfInterest(clientAreaOfInterest) {
	var c = document.getElementById("mask");
	var ctx = c.getContext("2d");
	ctx.beginPath();
	ctx.clearRect(0, 0, c.width, c.height);
	ctx.lineWidth = 5;
	ctx.strokeStyle="#FF8C00";
	ctx.rect(clientAreaOfInterest.x0, clientAreaOfInterest.y0, clientAreaOfInterest.width, clientAreaOfInterest.height); // Paint area of Interest box
	ctx.stroke();
	ctx.fillStyle = "rgba(255, 140, 0, 0.4)";
	ctx.rect(0, 0, c.width, c.height)
	ctx.fill();
	ctx.clearRect(clientAreaOfInterest.x0 + 2.5, clientAreaOfInterest.y0 + 2.5, clientAreaOfInterest.width - 5, clientAreaOfInterest.height - 5); // Clear inside rectangle
	ctx.closePath();
    }

    function isInsideCanvas(clientAreaOfInterest) {
	return (clientAreaOfInterest.x0 > 0 
	    && clientAreaOfInterest.x1 < maskCanvas.width 
	    && clientAreaOfInterest.y0 > 0
	    && clientAreaOfInterest.y1 < maskCanvas.height);
    }

    function isInsideBox(mouseX, mouseY, box) {
	return (box.x0 < mouseX && box.y0 < mouseY && box.x1 > mouseX && box.y1 > mouseY);
    }

    function serverAoiFromClientAoi(clientAreaOfInterest, imageWidth, imageHeight) {
        var xScaling = imageWidth / ctrl.maskCanvas.width;
        var yScaling = imageHeight / ctrl.maskCanvas.height;

	var x0 = clientAreaOfInterest.x0 * yScaling;
	var y0 = clientAreaOfInterest.y0 * xScaling;

	var width = clientAreaOfInterest.width * yScaling;
	var height = clientAreaOfInterest.height * xScaling;

	return {x0: x0, y0: y0, x1: x0 + width, y1: y0 + height};
    }

    function cleanAll() {
	console.log("clean all");
	cleanAreaOfInterest();
	cleanProductFlow();
	cleanClientPointsOfInterest();
	cleanAdjustContrast();
	cleanRecordVideo();
    }

    function cleanRecordVideo() {
	$('#recordVideoContainer').css('display', 'none');
	$('#recordVideoTools').css('display', 'none');
    }
    
    function cleanAdjustContrast() {
	$('#adjustContrastTools').css('display', 'none');
	$('#hsvContainer').css('display', 'none');
    }
    
    function cleanClientPointsOfInterest() {
	$('#pointsOfInterest').css('display', 'none');
	var maskContext = ctrl.maskCanvas.getContext('2d');
	maskContext.clearRect(0, 0, ctrl.maskCanvas.width, ctrl.maskCanvas.height); // Clear inside rectangle
	document.getElementById('mask').removeEventListener('mousedown', eventMouseDownPoI);
	document.getElementById('mask').removeEventListener('mousemove', eventMouseMovePoI);
	document.getElementById('mask').removeEventListener('mouseup', eventMouseUpPoI);
    }
    
    function cleanProductFlow() {
	$('#productFlow').css('display', 'none');
	delete ctrl.flowAoI;
	ctrl.flowAoI = null;
	var maskContext = ctrl.maskCanvas.getContext('2d');
	maskContext.clearRect(0, 0, ctrl.maskCanvas.width, ctrl.maskCanvas.height); // Clear inside rectangle
    }
    
    function cleanAreaOfInterest() {
	delete clientAreaOfInterest;
	clientAreaOfInterest = null;
	var maskContext = ctrl.maskCanvas.getContext('2d');
	maskContext.clearRect(0, 0, ctrl.maskCanvas.width, ctrl.maskCanvas.height); // Clear inside rectangle
	$('#areaOfInterestTools').css('display', 'none');
	$('#mask').unbind();
    }

    function setFlowDirection(selection, init) {
        if(selection == A){
            flowA = true;
            flowB = false;
            flowC = false;
            flowD = false;
        }
        else if(selection == B){
            flowA = false;
            flowB = true;
            flowC = false;
            flowD = false;
        }
        else if(selection == C){
            flowA = false;
            flowB = false;
            flowC = true;
            flowD = false;
        }
        else if(selection == D){
            flowA = false
            flowB = false;
            flowC = false;
            flowD = true
        }

	if (init === false) {
	    var sendData = {};
	    sendData['productFlow'] = selection;
	    PlutoREST.setConfig("t-cam.0", sendData).then(function(){
		console.log('Config saved successfully');
	    });
	}
    };

    function isInsideFlowBox(mouseX, mouseY, flowBox) {
        return (flowBox.x0 < mouseX && flowBox.y0 < mouseY && flowBox.x1 > mouseX && flowBox.y1 > mouseY);
    }

    function updateX(i, x) {
	console.log("updating x");
        clientPoI[i].x = x;
        sendPoI(clientPoI[i], i);
    }
    
    //Function for updating Y coordinate
    function updateY(i, y) {
	console.log("updating y");
        clientPoI[i].y = y;
        sendPoI(clientPoI[i], i);		
    }

    function updateP(i, x, y) {
	console.log("updating point");
        clientPoI[i].x = x;
	serverPoI[i].x = x * xScaling;
        clientPoI[i].y = y;
	serverPoI[i].y = y * yScaling;
        sendPoI(clientPoI[i], i);
    }

    function updateClientPoints() {
        PlutoREST.readConfig("POI.0").then(function(config) {
            ctrl.pointNumber = config["nPOI"];
            var xScaling = ctrl.maskCanvas.width / currentImage.width;
            var yScaling = ctrl.maskCanvas.height / currentImage.height;
            
            if(ctrl.pointNumber > clientPoI.length)
            {
                //update all points
                for (var i = 0; i < clientPoI.length; i++)
                {
                    serverPoI[i].x = config["POIx" + i];
                    serverPoI[i].y = config["POIy" + i];
                    clientPoI.x = config["POIx" + i] * xScaling;
                    clientPoI.y = config["POIy" + i] * yScaling;
                 
                }

                //Add new points up to new number of points
                for(var i = clientPoI.length; i < ctrl.pointNumber; i++)
                {
                    serverPoI.push(new POI(i, config["POIx" + i], config["POIy" + i]));
                    clientPoI.push(new POI(i, config["POIx" + i] * xScaling, config["POIy" + i] * yScaling));   
                } 
            }
            else if( ctrl.pointNumber < clientPoI.length)
            {
                //update all points
                for (var i = 0; i < ctrl.pointNumber; i++)
                {
                    serverPoI[i].x = config["POIx" + i];
                    serverPoI[i].y = config["POIy" + i];
                    clientPoI.x = config["POIx" + i] * xScaling;
                    clientPoI.y = config["POIy" + i] * yScaling;
                }
                
                //Remove points higher than ctrl.pointNumber
                while( clientPoI.length > ctrl.pointNumber)
                {
                    clientPoI.splice(-1,1);
                    serverPoI.splice(-1,1);
                }

            }
	    ctrl.maskCanvas.addEventListener('mousedown', eventMouseDownPoI);
            drawPoI(clientPoI);
        });
    }

    function sendPoI(p, i) {
        var sendData = {};
        sendData["POIx" + i] = p.x * xScaling;
        sendData["POIy" + i] = p.y * yScaling;
        PlutoREST.setConfig("POI.0", sendData).then(function() {
	    console.log("Data sent to server config.");
        });
    }

    function isClicked(point, mousex, mousey) {
        xpow = Math.pow((point.x - mousex), 2);
        ypow = Math.pow((point.y - mousey), 2);
        if (Math.sqrt(xpow + ypow) <= 10) {
            return true;
        } else {
	    return false;
	}
    }

    function drawPoI(pointArray) {
        maskCanvas = document.getElementById('mask');
        maskContext = maskCanvas.getContext('2d'); 
        maskContext.clearRect(0, 0, maskCanvas.width, maskCanvas.height);
        for(i = 0; i < clientPoI.length; i++){
            maskContext.beginPath();
            //Fill color is set transparent as POI numbers
            //are filled in with images from back end
            maskContext.fillStyle = 'rgba(255, 0, 0, 0)'; // "#FF8C00";
            maskContext.arc(clientPoI[i].x, clientPoI[i].y, 10, 0, 2 * Math.PI);
            maskContext.fill();
            maskContext.font = "24px Arial Black";
            var s = 30;
            if (clientPoI[i].y > maskCanvas.height/2){
                s -= 45;
            }
            maskContext.fillText(i, clientPoI[i].x - 8, clientPoI[i].y + s);
        }
    }

    function initVariables(config) {
	// Area of Interest
	x0 = config["AOIx0"];
	y0 = config["AOIy0"];
	x1 = config["AOIx1"];
	y1 = config["AOIy1"];
	serverAreaOfInterest = new AreaOfInterest(x0, y0, x1 - x0, y1 - y0);

	// Adjust Contrast (Edit Product Filter)
	ctrl.hMin = config["minHue"];
	ctrl.hMax = config["maxHue"];
	ctrl.sMin = config["minSaturation"];
	ctrl.sMax = config["maxSaturation"];
	ctrl.vMin = config["minValue"];
	ctrl.vMax = config["maxValue"];

	// Product Flow
	ctrl.flowDirection = config["productFlow"];
	setFlowDirection(ctrl.flowDirection);
    }

    function setFileNames(){
        console.log("Files lenght: " + ctrl.videoFiles.length);
        ctrl.selectedVideo = ctrl.videoFiles[ctrl.videoFiles.length - 1];
        widgetService.InputOption.videoSelector.setValue(ctrl.selectedVideo);
        widgetService.InputOption.videoSelector.setOptions([]);
        widgetService.InputOption.videoSelector.setOptions(ctrl.videoFiles);
        changeVideo(ctrl.selectedVideo);
    }

    //Function called to update select list
    //for video recordings
    function updateVideoList() {
        var getUrl = "http://localhost/t-cam-video/files.json";
        $scope.videoFiles = [];
        ctrl.videoFiles = [];
        ctrl.selectedVideo = "Select video";
        var i = 0;
        $.getJSON(getUrl, function(data){
            var video;
            for (video in data) {
                if (data.hasOwnProperty(video)) {
                    //ctrl.videoFiles[i++] = data[video];
                    ctrl.videoFiles.push(data[video]);
                    console.log("Video file: " + ctrl.videoFiles[i++]);
                }
            }
            setFileNames();
        });
        return ctrl.selectedVideo;      
    };


    //Called to show user that recording is taking place
    //displays text ontop of video feed
    function showWhenRecording(timeout) {
        var div = document.getElementById("showWhileRecording");
        //change msec to sec
        timeout = timeout * 1000;
        
        document.getElementById("showWhileRecording").style.display ="";

        setTimeout(function(){
            document.getElementById("showWhileRecording").style.display ="none";// "hidden";
        },timeout);
    }    
 
    function changeVideo(selection){
        if(selection == null){
            return;
        }
        else{
            video = document.getElementById('videoPlayer');
            video.src = "http://localhost/t-cam-video/" + selection;
        }
    };

    // // // // // // // // // // // // // // // // //
    //              Button Actions 
    // // // // // // // // // // // // // // // // //

    $scope.flowDirections = [A, B, C, D];
 
    $scope.tellMe = function(selection) {
	var maskContext = ctrl.maskCanvas.getContext('2d');
	maskContext.clearRect(0, 0, ctrl.videoCanvas.width, ctrl.videoCanvas.height);
        setFlowDirection(selection);
	drawFlowAoI(currentImage);
    };

    ctrl.PoIChanged = function(val) {
        widgetService.InputSpinner.switchPoints.setMaxValue(val-1);
        var sendData = {};
        sendData["nPOI"] = val;
        ctrl.pointNumber = val;
        PlutoREST.setConfig("POI.0", sendData).then(function(){
            updateClientPoints();
        });
        if(ctrl.switchPoints > ctrl.pointNumber-1) {
           ctrl.switchPoints = ctrl.pointNumber-1;
        }
        ctrl.switchPointsMaxValue = ctrl.pointNumber-1;
    }

    ctrl.onChangeSwitchPoints = function(val) {
	selectedPoint = val;
        PlutoREST.readConfig("POI.0").then(function(config) {
            ctrl.pointNumber = config["nPOI"];
            if(val > ctrl.pointNumber){
                return;
            }
            var xScaling = ctrl.maskCanvas.width / currentImage.width;
            var yScaling = ctrl.maskCanvas.height / currentImage.height;
            ctrl.xValue = config["POIx" + val] * xScaling;
            ctrl.yValue = config["POIy" + val] * yScaling;
        });
    }

    ctrl.onChangeXValue = function(val) {
        selectedPoint = ctrl.switchPoints;
        var sendData = {};
        sendData["POIx" + selectedPoint] = val * xScaling;
        PlutoREST.setConfig("POI.0", sendData).then(function() {
	    clientPoI[selectedPoint].x = val;
	    drawPoI();
        });
    }

    ctrl.onChangeYValue = function(val) {
        selectedPoint = ctrl.switchPoints;
        var sendData = {};
        sendData["POIy" + selectedPoint] = val * yScaling;
        PlutoREST.setConfig("POI.0", sendData).then(function() {
            console.log("Safe y value");
        });
        updateClientPoints();
        //serverPoI[selectedPoint].y = val * yScaling;
        //clientPoI[selectedPoint].y = val;
        drawPoI(clientPoI);

    }
    
    ctrl.aoiXchanged = function(val) {
	var clientValue = val / xScaling; 
	if (isAreaWithinBounds(clientValue, clientAreaOfInterest.y0, clientAreaOfInterest.width, clientAreaOfInterest.height)) {
	    var sendData = {};
	    sendData['AOIx0'] = val;
	    PlutoREST.setConfig("t-cam.0", sendData).then(function(){
		serverAreaOfInterest.x0 = val;
		widgetService.InputSpinner.aoiXchange.setValue(val);
		var newAOI = new AreaOfInterest(clientValue,
						clientAreaOfInterest.y0,
						clientAreaOfInterest.width,
						clientAreaOfInterest.height);
		delete clientAreaOfInterest;
		clientAreaOfInterest = newAOI;
		drawAreaOfInterest(clientAreaOfInterest);
		updateInputSpinnersAOI(serverAreaOfInterest);
	    });
	}
    }

    ctrl.aoiYchanged = function(val) {
	var clientValue = val / yScaling; 
	if (isAreaWithinBounds(clientAreaOfInterest.x0, clientValue, clientAreaOfInterest.width, clientAreaOfInterest.height)) {
	    var sendData = {};
	    sendData['AOIy0'] = val;

	    PlutoREST.setConfig("t-cam.0", sendData).then(function(){
		serverAreaOfInterest.y0 = val;
		widgetService.InputSpinner.aoiYchange.setValue(val);
		var newAOI = new AreaOfInterest(clientAreaOfInterest.x0,
						clientValue,
						clientAreaOfInterest.width,
						clientAreaOfInterest.height);
		delete clientAreaOfInterest;
		clientAreaOfInterest = newAOI;
		drawAreaOfInterest(clientAreaOfInterest);
		updateInputSpinnersAOI(serverAreaOfInterest);
	    });
	}
    }

    ctrl.aoiWidthChanged = function(val) {
	var clientValue = val / xScaling; 
	if (isAreaWithinBounds(clientAreaOfInterest.x0, clientAreaOfInterest.y0, clientValue, clientAreaOfInterest.height)) {
	    var sendData = {};

	    sendData['AOIx1'] = serverAreaOfInterest.x0 + val;

	    PlutoREST.setConfig("t-cam.0", sendData).then(function(){
		serverAreaOfInterest.x1 = serverAreaOfInterest.x0 + val;
		widgetService.InputSpinner.aoiWidthChange.setValue(val);

		var newAOI = new AreaOfInterest(clientAreaOfInterest.x0,
						clientAreaOfInterest.y0,
						clientValue,
						clientAreaOfInterest.height);
		delete clientAreaOfInterest;
		clientAreaOfInterest = newAOI;
		drawAreaOfInterest(clientAreaOfInterest);
		updateInputSpinnersAOI(serverAreaOfInterest);
	    });
	}
    }
    
    ctrl.aoiHeightChanged = function(val) {
	var clientValue = val / yScaling; 
	if (isAreaWithinBounds(clientAreaOfInterest.x0, clientAreaOfInterest.y0, clientAreaOfInterest.width, clientValue)) {
	    var sendData = {};
	    sendData['AOIy1'] = serverAreaOfInterest.y0 + val;

	    PlutoREST.setConfig("t-cam.0", sendData).then(function(){
		serverAreaOfInterest.y1 = serverAreaOfInterest.y0 + val;
		widgetService.InputSpinner.aoiHeightChange.setValue(val);
		var newAOI = new AreaOfInterest(clientAreaOfInterest.x0,
						clientAreaOfInterest.y0,
						clientAreaOfInterest.width,
						clientValue);
		delete clientAreaOfInterest;
		clientAreaOfInterest = newAOI;
		drawAreaOfInterest(clientAreaOfInterest);
		updateInputSpinnersAOI(serverAreaOfInterest);
	    });
	}
    }

   ctrl.spinnerValuehMinChanged = function(val) {
        console.log("Val hMin " +  val);
         var sendData = {};
         //Set configuration storage
        sendData['minHue'] = val;
        PlutoREST.setConfig("t-cam.0",sendData).then(function(){
            console.log('Config saved successfully');
       });
       PlutoVariables.publish("integer.wsserver.0.hMin",val);
       widgetService.StatusBar.global.setStatusMessage("hMax changed to: '" + val + "'");
    };

    ctrl.HSVMinValue = 0;
    ctrl.HSVMaxValue = 256;   

   ctrl.spinnerValuehMaxChanged = function(val) {
        console.log("Val hMax " +  val);
        var sendData = {};
        //Set configuration storage
        sendData['maxHue'] = val;
        PlutoREST.setConfig("t-cam.0",sendData).then(function(){
            console.log('Config saved successfully');
        });
        PlutoVariables.publish("integer.wsserver.0.hMax",val);
        widgetService.StatusBar.global.setStatusMessage("hMax changed to: '" + val + "'");
    };
   ctrl.spinnerValuesMinChanged = function(val) {
        console.log("Val sMin " +  val);
        var sendData = {};
        //Set configuration storage
        sendData['minSaturation'] = val;
        PlutoREST.setConfig("t-cam.0",sendData).then(function(){
            console.log('Config saved successfully');
        });
        PlutoVariables.publish("integer.wsserver.0.sMin",val);
        widgetService.StatusBar.global.setStatusMessage("sMin changed to: '" + val + "'");
    };
   ctrl.spinnerValuesMaxChanged = function(val) {
        console.log("Val sMax " +  val);
        var sendData = {};
         //Set configuration storage
        sendData['maxSaturation'] = val;
        PlutoREST.setConfig("t-cam.0",sendData).then(function(){
            console.log('Config saved successfully');
        });       PlutoVariables.publish("integer.wsserver.0.sMax",val);
        widgetService.StatusBar.global.setStatusMessage("sMax changed to: '" + val + "'");
    };
   ctrl.spinnerValuevMinChanged = function(val) {
        console.log("Val vMin " +  val);
        var sendData = {};
        //Set configuration storage
        sendData['minValue'] = val;
        PlutoREST.setConfig("t-cam.0",sendData).then(function(){
            console.log('Config saved successfully');
        });       
        PlutoVariables.publish("integer.wsserver.0.vMin",val);
        widgetService.StatusBar.global.setStatusMessage("vMin changed to: '" + val + "'");
   };
   ctrl.spinnerValuevMaxChanged = function(val) {
        console.log("Val vMax " +  val);
        var sendData = {};
        //Set configuration storage
        sendData['maxValue'] = val;
        PlutoREST.setConfig("t-cam.0",sendData).then(function(){
            console.log('Config saved successfully');
        });       
        PlutoVariables.publish("integer.wsserver.0.vMax",val);
        widgetService.StatusBar.global.setStatusMessage("vMax changed to: '" + val + "'");
    };

    ctrl.recordVideo = function(widgetName){
        console.log("Save");
        var sendData = {};

        sendData['record'] = 1;

	console.log(sendData);
        PlutoREST.setConfig("t-cam.0",sendData).then(function(){
             console.log('Config saved successfully');
        })
        .catch(function(error){
             console.error("Could not start recording, error returned: " + error.ErrorCode + ' : ' + error.ErrorMsg);
        });
        console.log("Calling you " + this.videoLength); 
        showWhenRecording(ctrl.videoLength);

        //update list after video has recorded
        var timer = (ctrl.videoLength) * 1000 + 2000;
        setTimeout(function(){
            changeVideo(updateVideoList());
        },timer);
    };

    ctrl.refreshVideo = function(widgetName){
        console.log("Refresh");

        video = document.getElementById('videoPlayer');
        if(videoSwap == 0)
        {
            video.src = "http://localhost/t-cam-video/TCAMrecording1.ogg";
            videoSwap = 1;
            console.log("http://localhost/t-cam-video/TCAMrecording1.ogg");
        }
        else
        {
            video.src = "http://localhost/t-cam-video/TCAMrecording2.ogg"
            videoSwap = 0;
            console.log('http://localhost/t-cam-video/TCAMrecording2.ogg');
        }
        video.pause();
        video.load();
        video.play();

    };

    ctrl.videoLengthChanged = function (val) {

	console.log("Value Video length cahnged to" +  val);
         
        var sendData = {};
	//Set configuration storage
	sendData['videoLength'] = val;
        this.videoLength = val;
	PlutoREST.setConfig("t-cam.0",sendData).then(function(){
	    console.log('Config saved successfully');
	});

	// implement what do do when value is changed
	widgetService.StatusBar.global.setStatusMessage("Value changed to: " + val);

    };

    $scope.onVideoSelect = function(selection) {
        changeVideo(selection);
    };


    $scope.loadView = function(view) {
	console.log("Button Pressed For: " + view);
	if (view === 'BaseView') {

	// // // // // // // // // // // // // // // // //
	//              Home 
	// // // // // // // // // // // // // // // // //

	    PlutoREST.readConfig("t-cam.0").then(function(config){
		initVariables(config);
		$('#mainVideoContainer').css('display', 'block');
		setImageFeed('rawFeed');
		cleanAll();
	    });

	} else if (view === 'AreaOfInterest') {

	// // // // // // // // // // // // // // // // //
	//              Area of Interest
	// // // // // // // // // // // // // // // // //

	    PlutoREST.readConfig("t-cam.0").then(function(config){
		cleanAll();
		initVariables(config);
		$('#mainVideoContainer').css('display', 'block');
		$('#areaOfInterestTools').css('display', 'block');
		setImageFeed('tcamFeed');
		initClientAreaOfInterest(serverAreaOfInterest,
					 currentImage.width,
					 currentImage.height);
	    });

	} else if (view === 'ProductFlow') {

	// // // // // // // // // // // // // // // // //
	//              Product Flow 
	// // // // // // // // // // // // // // // // //

	    PlutoREST.readConfig("t-cam.0").then(function(config){
		cleanAll();
		initVariables(config);
		$('#mainVideoContainer').css('display', 'block');
		setImageFeed('rawFeed');
		initializeFlowBoxes();
		drawFlowAoI(currentImage);
		$('#productFlow').css('display', 'block');
	    });

	} else if (view === 'PointsOfInterest') {

	// // // // // // // // // // // // // // // // //
	//              Points of Interest 
	// // // // // // // // // // // // // // // // //

	    PlutoREST.readConfig("POI.0").then(function(config){
		cleanAll();
		initVariables(config);
		$('#mainVideoContainer').css('display', 'block');
		setImageFeed('tcamFeed');
		$('#pointsOfInterest').css('display', 'block');
                //Points of interest
                ctrl.pointNumber = config["nPOI"];
		initializePoI(config);


	    });
        
	} else if (view === 'AdjustContrastView') {

	// // // // // // // // // // // // // // // // //
	//              Adjust Contrast 
	// // // // // // // // // // // // // // // // //

	    PlutoREST.readConfig("t-cam.0").then(function(config){
		cleanAll();
		initVariables(config);
		setImageFeed('hsvFeed');
		$('#hsvContainer').css('display', 'block');
		$('#adjustContrastTools').css('display', 'block');
		$('#mainVideoContainer').css('display', 'none');
	    });

	} else if (view === 'RecordVideoView') {

	// // // // // // // // // // // // // // // // //
	//              Record Video 
	// // // // // // // // // // // // // // // // //

	    PlutoREST.readConfig("t-cam.0").then(function(config){
		cleanAll();
		initVariables(config);
		setImageFeed('recordSource');
		$('#mainVideoContainer').css('display', 'none');
		$('#recordVideoContainer').css('display', 'block');
		$('#recordVideoTools').css('display', 'block');
                // Update widgets and values
                this.videoLength = config["videoLength"];
	        if(this.videoLength < 0)
	        {
		    this.videoLength = 0;
                }       
                console.log("Video length is " + this.videoLength);
	        widgetService.InputNumber.videoLength.setValue(this.videoLength);
	        widgetService.InputNumber.videoLength.setIcon("production");
	        widgetService.InputNumber.videoLength.setDecimals(0);
	        widgetService.InputNumber.videoLength.setHelp("Select video recording length");
	    })
            updateVideoList();   
	}
    }

}]);
