
angular.module('GuiApp')
.controller('TCAMView', ['PlutoVariables', 'PlutoWebSocket', 'PlutoREST', function(PlutoVariables, PlutoWebSocket, widgetService, PlutoREST){


    //Variables
    var ctrl = this;

    //Pluto pin  subscriptions
    var subscription_handle_tCamIP = null;

    // Subscribe to adressIP
    subscription_handle_tCamIP = PlutoVariables.subscribe('string.wsserver.0.tCamIP' , function(tCamIP) {
                    ctrl.tCamIP = tCamIP;
                    console.log(ctrl.tCamIP);
     });

    var previewImage = new Image();
/*
    var ipAddress = null;
    var port = null;
    var config {};

    PlutoREST.readConfig("t-cam.0").then(function(config){
        ipAddress = config["TcamIP"];
        port = config["TcamPort"];
    })
    .catch(function(error){
        console.error("Could not get name of Production Screen, error returned: "
                                 + error.ErrorCode + ' : ' + error.ErrorMsg);
    }); 

        var ip = ipAddress + ":" + port;
console.log(ip);
//    var pws = new PlutoWebSocket(ip);


*/
    //Set server IP address
    //var pws = new PlutoWebSocket("10.100.11.106:8183");
    var pws = new PlutoWebSocket("localhost:8183");
    //Message when connection to host is established
    pws.onConnect(function() {
        console.log("CONNECTED");
    });
    var dataLoaded = false;

    pws.onMessage(function(msg) {

        console.log("MESSAGE");         
        var jdata = angular.fromJson(msg);      
        if ( jdata.productionData != null)
        { 
            previewImage.src = "data:image/jpeg;base64," + jdata.productionData;       
            if(previewImage.width != 0) 
            {
                displayImage(previewImage);
            }
        }
    });

    pws.connect();

    var coordScaling = 1;
    var canvas = null;
    var context = null;
    var maskCanvas = null;
    var maskContext = null;
    var containerWidth = 0;
    var containerHeight = 0;
    var wrh = 1;
    var newWidth = 5;
    var newHeight = 5;
  

    function displayImage(img){


        //Cet canvas info and set size parameters
        //according to received image 
        if (canvas === null) {

            canvas = document.getElementById('baseview');
            context = canvas.getContext('2d');
            containerWidth = document.getElementById('canvasContainer').clientWidth;
            containerHeight = document.getElementById('canvasContainer').clientHeight;
            coordScaling = img.width / containerWidth;
            wrh = img.width / img.height;
            newWidth = containerWidth;
            newHeight = newWidth / wrh; 

            if (newHeight > containerHeight) {
                newHeight = containerHeight;
                newWidth = newHeight * wrh;
                coordScaling = img.height / containerHeight;
            }

            canvas.width = newWidth;
            canvas.height = newHeight;
        }
        context.drawImage(img, 0, 0, newWidth, newHeight);
        img = "";
    }

    $(window).bind('resize', canvasReSize);

    function canvasReSize(){

        canvas = document.getElementById('baseview');
        context = canvas.getContext('2d');
        containerWidth = $(window).width();
        containerHeight = $(window).width();


        coordScaling = previewImage.width / containerWidth;
        wrh = previewImage.width / previewImage.height;
        newWidth = containerWidth;
        newHeight = newWidth / wrh;

        if (newHeight > containerHeight) {
            newHeight = containerHeight;
            newWidth = newHeight * wrh;
            coordScaling = previewImage.height / containerHeight;
        }

        canvas.width = newWidth;
        canvas.height = newHeight;
    };

     //Unsubscribe
     ctrl.routerOnHide = function() {
	 PlutoVariables.unsubscribe(subscription_handle_tCamIP);
    }
    
}]);

