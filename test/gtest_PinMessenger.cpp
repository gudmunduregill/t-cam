#include <pluto/pins/mocks/dio.hpp>
#include <PinMessenger.hpp>
#include <gtest/gtest.h>
#include <gmock/gmock.h>


// The code you are testing
class PinMessengerTest : public PinMessenger
{
    public:
        PinMessengerTest(){  }
        //PinMessengerTest(int i){tag = i; }
        ~PinMessengerTest(){  }

        PinMessengerTest* pinMessengerTest;

    private:
        int tag;

        virtual void SetUp()
        {
            //PinMessengerTest pinMessengerTest;
             pinMessengerTest = new PinMessengerTest();
        }

        virtual void TearDown()
        {
            delete pinMessengerTest;
        }

    
};





TEST(Mocking, setPOI0)
{
    // setup expectations

    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    EXPECT_CALL(pinMessengerTest->_POI00, set(false,testing::_)).Times(1);

    pinMessengerTest->setPOI(0, false);
    delete pinMessengerTest;
}


TEST(Mocking, setPOI1)
{
    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    // setup expectations
    EXPECT_CALL(pinMessengerTest->_POI01, set(false,testing::_)).Times(1);

    pinMessengerTest->setPOI(1, false);
    delete pinMessengerTest;
}

TEST(Mocking, setPOI2)
{
    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    // setup expectations
    EXPECT_CALL(pinMessengerTest->_POI02, set(false,testing::_)).Times(1);

    pinMessengerTest->setPOI(2, false);
    delete pinMessengerTest;
}

TEST(Mocking, setPOI3)
{
    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    // setup expectations
    EXPECT_CALL(pinMessengerTest->_POI03, set(false,testing::_)).Times(1);

    pinMessengerTest->setPOI(3, false);
    delete pinMessengerTest;
}

TEST(Mocking, setPOI4)
{
    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    // setup expectations
    EXPECT_CALL(pinMessengerTest->_POI04, set(false,testing::_)).Times(1);

    pinMessengerTest->setPOI(4, false);
    delete pinMessengerTest;
}

TEST(Mocking, setPOI5)
{
    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    // setup expectations
    EXPECT_CALL(pinMessengerTest->_POI05, set(false,testing::_)).Times(1);

    pinMessengerTest->setPOI(5, false);
    delete pinMessengerTest;
}

TEST(Mocking, setPOI6)
{
    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    // setup expectations
    EXPECT_CALL(pinMessengerTest->_POI06, set(false,testing::_)).Times(1);

    pinMessengerTest->setPOI(6, false);
    delete pinMessengerTest;
}

TEST(Mocking, setPOI7)
{
    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    // setup expectations
    EXPECT_CALL(pinMessengerTest->_POI07, set(false,testing::_)).Times(1);

    pinMessengerTest->setPOI(7, false);
    delete pinMessengerTest;
}

TEST(Mocking, messagePOI)
{
    PinMessengerTest* pinMessengerTest = new PinMessengerTest();
    // setup expectations
    EXPECT_CALL(pinMessengerTest->_POI00, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI01, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI02, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI03, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI04, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI05, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI06, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI07, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI08, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI09, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI10, set(false,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI11, set(false,testing::_)).Times(1);

    EXPECT_CALL(pinMessengerTest->_POI00, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI01, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI02, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI03, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI04, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI05, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI06, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI07, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI08, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI09, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI10, set(true,testing::_)).Times(1);
    EXPECT_CALL(pinMessengerTest->_POI11, set(true,testing::_)).Times(1);



    for (int i = 0; i < 12; i++)
    {
        pinMessengerTest->messagePOI(i, "close", 12);
        pinMessengerTest->messagePOI(i, "open", 12);
    }
    delete pinMessengerTest;
}

