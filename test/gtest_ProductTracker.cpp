#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <appcbase.h>
#include <mqs_queue.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>


#include <iostream>
#include <fstream>

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

#include <ProductTracker.hpp>
#include "Config.hpp"
#include "Product.hpp"

using namespace std;
using namespace testing;

class MockConfig : public Config
{
public:
	MockConfig() : Config() {};
        MockConfig(int i) { }; 
	MOCK_METHOD0(getAOIx0, int64_t());
	MOCK_METHOD0(getAOIy0, int64_t());
	MOCK_METHOD0(getAOIx1, int64_t());
	MOCK_METHOD0(getAOIy1, int64_t());
	MOCK_METHOD0(getProductFlow, string());
	MOCK_METHOD0(getEntranceTolerance, int64_t());
}
;

class MockProduct : public Product
{
public:
        MockProduct() : Product() {};
        MOCK_METHOD0(getXPos, int());
        MOCK_METHOD0(getYPos, int());
}
;



class ProductTrackerTest : public testing::Test
{
    public:
        ProductTracker* productTracker;
        MockConfig* mockConfig;
   
    	ProductTrackerTest()
	{

        }

        ~ProductTrackerTest()
        {
           //delete productTracker;
           //delete mockConfig;
        }

        virtual void SetUp()
        {
            productTracker = new ProductTracker();
            mockConfig = new MockConfig(0);

            EXPECT_CALL(*mockConfig, getAOIx0()).WillRepeatedly(Return(0));
            EXPECT_CALL(*mockConfig, getAOIy0()).WillRepeatedly(Return(0));
            EXPECT_CALL(*mockConfig, getAOIx1()).WillRepeatedly(Return(512));
            EXPECT_CALL(*mockConfig, getAOIy1()).WillRepeatedly(Return(512));
            EXPECT_CALL(*mockConfig, getEntranceTolerance()).WillRepeatedly(Return(60));
        
        }

        virtual void TearDown()
        {
            delete productTracker;
            delete mockConfig;
        }

};


TEST_F(ProductTrackerTest, isAtEntranceR2L)
{

    EXPECT_CALL(*mockConfig, getProductFlow()).WillRepeatedly(Return("Flow right to left"));
    vector<Product> foundProducts;
    Mat zeroMat = Mat(512,512, CV_8UC1, double(0));
    Mat thresholdFrame;// = (zeroMat | oneMat);
    VideoCapture capture;
    capture.open("/home/pluto/t-cam/test/R2L.tif");

    capture >> thresholdFrame;
    cvtColor(thresholdFrame, thresholdFrame, CV_BGR2GRAY);


    //get products from image
    foundProducts = productTracker->findObjects(thresholdFrame, zeroMat, mockConfig->getProductFlow());

    EXPECT_TRUE(productTracker->isAtEntrance(foundProducts[0], (Config*) mockConfig));
}


TEST_F(ProductTrackerTest, isAtEntranceL2R)
{
    EXPECT_CALL(*mockConfig, getProductFlow()).WillRepeatedly(Return("Flow left to right"));
    vector<Product> foundProducts;
    Mat zeroMat = Mat(512,512, CV_8UC1, double(0));
    Mat thresholdFrame;// = (zeroMat | oneMat);
    VideoCapture capture;
    capture.open("/home/pluto/t-cam/test/L2R.tif");

    capture >> thresholdFrame;
    cvtColor(thresholdFrame, thresholdFrame, CV_BGR2GRAY);

    
    
    //get products from image
    foundProducts = productTracker->findObjects(thresholdFrame, zeroMat, mockConfig->getProductFlow());

    EXPECT_TRUE(productTracker->isAtEntrance(foundProducts[0], (Config*) mockConfig));
}

TEST_F(ProductTrackerTest, isAtEntranceT2B)
{
    EXPECT_CALL(*mockConfig, getProductFlow()).WillRepeatedly(Return("Flow top to bottom"));
    vector<Product> foundProducts;
    Mat zeroMat = Mat(512,512, CV_8UC1, double(0));
    Mat thresholdFrame;// = (zeroMat | oneMat);
    VideoCapture capture;
    capture.open("/home/pluto/t-cam/test/T2B.tif");

    capture >> thresholdFrame;
    cvtColor(thresholdFrame, thresholdFrame, CV_BGR2GRAY);

    //get products from image
    foundProducts = productTracker->findObjects(thresholdFrame, zeroMat, mockConfig->getProductFlow());

    EXPECT_TRUE(productTracker->isAtEntrance(foundProducts[0], (Config*) mockConfig));

}

TEST_F(ProductTrackerTest, isAtEntranceB2T)
{
    EXPECT_CALL(*mockConfig, getProductFlow()).WillRepeatedly(Return("Flow bottom to top"));
    vector<Product> foundProducts;
    Mat zeroMat = Mat(512,512, CV_8UC1, double(0));
    Mat thresholdFrame;// = (zeroMat | oneMat);
    VideoCapture capture;
    capture.open("/home/pluto/t-cam/test/B2T.tif");

    capture >> thresholdFrame;
    cvtColor(thresholdFrame, thresholdFrame, CV_BGR2GRAY);


    //get products from image
    foundProducts = productTracker->findObjects(thresholdFrame, zeroMat, mockConfig->getProductFlow());

    EXPECT_TRUE(productTracker->isAtEntrance(foundProducts[0], (Config*) mockConfig));

}

TEST_F(ProductTrackerTest, findOneObject)
{
    vector<Product> foundProducts;
    Mat zeroMat = Mat(512,512, CV_8UC1, double(0));
    Mat oneMat = Mat(512,512, CV_8UC1, double(0));
    bitwise_not(oneMat, oneMat);
    Mat thresholdFrame = (zeroMat | oneMat);
    VideoCapture capture;
    capture.open("/home/pluto/t-cam/test/blob.tif");

    capture >> thresholdFrame;
    cvtColor(thresholdFrame, thresholdFrame, CV_BGR2GRAY); 
    foundProducts = productTracker->findObjects(thresholdFrame, zeroMat, mockConfig->getProductFlow());
    EXPECT_TRUE(foundProducts.size() == 1);
}


