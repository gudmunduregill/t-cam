#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <appcbase.h>
#include <mqs_queue.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>


#include <iostream>
#include <fstream>

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

#include "ProductionViewController.hpp"

using namespace std;
using namespace testing;


class ProductionViewControllerTest : public testing::Test
{
    public:
        ProductionViewController* _productionViewController;

   
    	ProductionViewControllerTest()
	{
            _productionViewController = new ProductionViewController();
        }

        ~ProductionViewControllerTest()
        {
            delete _productionViewController;
        }

};


TEST_F(ProductionViewControllerTest, findDistance)
{
    Point p1 = Point(0,0);
    Point p2 = Point(0,100);

    EXPECT_TRUE(_productionViewController->distance(p1, p2) == 100);
}
