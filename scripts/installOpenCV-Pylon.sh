#!/bin/bash
#OpenCV installation for Ubuntu 12.04
	sudo apt-get update
	sudo apt-get upgrade
# Pluto packages
	#Install all c++ pins
	sudo apt-get install integerpin-crossdevelopment
	sudo apt-get install syncpin-crossdevelopment
	sudo apt-get install digitaliopin-crossdevelopment
	sudo apt-get install stringpin-crossdevelopment
	sudo apt-get install doublepin-crossdevelopment
	sudo apt-get install measurementpin-crossdevelopment
	sudo apt-get install iteminfopin-crossdevelopment

 	#install remaining pluto packages
	sudo apt-get install iomcbench-crossdevelopment  
	sudo apt-get install browserwindow-crossdevelopment 
	sudo apt-get install lighttpd-crossdevelopment 
	sudo apt-get install magnet-crossdevelopment 
	sudo apt-get install wsserver-crossdevelopment 
	sudo apt-get install angularjs-crossdevelopment 
	sudo apt-get install websocket-crossdevelopment 
	sudo apt-get install wsclient-crossdevelopment 
	sudo apt-get install html-guiframework-crossdevelopment
	sudo apt-get install libboost-all-dev
#install git
	sudo apt-get install git
#To install OpenCV 2.4.11 on the Ubuntu 12.04 operating system, first install a developer environment to build OpenCV.
	sudo apt-get -y install build-essential cmake pkg-config
#Install Image I/O libraries
	sudo apt-get -y install libjpeg62-dev 
	sudo apt-get -y install libtiff4-dev libjasper-dev
#Install the GTK dev library
	sudo apt-get -y install  libgtk2.0-dev
#Install Video I/O libraries
	sudo apt-get -y install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
#Install Pylon package for camera
	sudo dpkg -i ~/t-cam/pylon/pylon_5.0.11.10914-deb0_i386.deb
	sudo apt-get install -f
#install Gstreamer and other codecs
	sudo apt-get install gstreamer0.10-ffmpeg
	sudo apt-get install ubuntu-restricted-extras
#Install VLC video player
	sudo apt-get install vlc
#Optional - install support for Firewire video cameras
	sudo apt-get -y install libdc1394-22-dev
#Optional - install video streaming libraries
	sudo apt-get -y install libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev 
#Optional - install the Python development environment and the Python Numerical library
	sudo apt-get -y install python-dev python-numpy
#Optional - install the parallel code processing library (the Intel tbb library)
	sudo apt-get -y install libtbb-dev
#Optional - install the Qt dev library
	sudo apt-get -y install libqt4-dev
#update and upgrade
	sudo apt-get update
	sudo apt-get upgrade
#Now download OpenCV 2.4 to wherever you want to compile the source.
	# download opencv-2.4.11
	#wget https://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.11/opencv-2.4.11.zip/download
    #mv download opencv-2.4.11.zip
    	wget https://github.com/opencv/opencv/archive/2.4.11.zip
	echo download complete
	unzip 2.4.11.zip -d /opt
	echo Unzip complete
	cd /opt/opencv-2.4.11
	mkdir build
	cd build
#Create and build directory and onfigure OpenCV with cmake. Don't forget the .. part at the end of cmake cmd !!
	cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=ON -D WITH_OPENGL=ON ..
#Now compile it
	make
#And finally install OpenCV
	sudo make install
#Set library path:
	ldconfig
	export LD_LIBRARY_PATH=/opt/opencv-2.4.11/build/lib
#Confirm installation
	pkg-config --modversion opencv
 



