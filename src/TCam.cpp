/**
 @file TCam.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */


#include <MarelCppApp.h>
#include "TCam.h"

#include <stdio.h>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>

using namespace cv;
using namespace std;


int main(int argc, char** argv)
{
    TCam* marelApp;


    marelApp = new TCam(argv[0]);
  

    if(marelApp)
    {   
        cout << "Marel app run.." << endl;
        int ret = marelApp->startUp(argc, argv);
        return ret;
    }

    return -1;
}

