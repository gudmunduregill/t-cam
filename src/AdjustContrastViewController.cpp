/**
 @file AdjustContrastViewController.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */

#include "AdjustContrastViewController.hpp"

AdjustContrastViewController::AdjustContrastViewController() { }
AdjustContrastViewController::~AdjustContrastViewController() { }

void AdjustContrastViewController::adjustContrast(Config* config, Frames* frames, osa_mutex_t mutex)
{
    Mat HSV;
    Mat res;

    //Lock and create next frame
    osa_mutex_lock(mutex,0);
        //convert frame from BGR to HSV colorspace
        cvtColor(frames->areaOfInterestFeed,HSV,COLOR_BGR2HSV);
        //filter HSV image between values and store filtered image to
        //threshold matrix
        inRange(HSV,Scalar(config->gethMin(), config->getsMin(), config->getvMin()),
                        Scalar(config->gethMax(), config->getsMax(), config->getvMax()), 
                        frames->thresholdImage);
            
        frames->cameraContrastFeed = Mat();
        bitwise_and(frames->areaOfInterestFeed, frames->areaOfInterestFeed,  frames->cameraContrastFeed,  frames->thresholdImage);
        frames->newAreaOfInterestImage = false;
        frames->newThresholdImage = true;
    //unlock mutex and continue
    osa_mutex_unlock(mutex);


    return;
}
