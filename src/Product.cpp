/**
 @file Product.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */
#include "Product.hpp"

Product::Product() { }

Product::Product(std::vector<cv::Point> _contour, string productFlow) 
{
    currentContour = _contour;
    boundingRect = cv::boundingRect(currentContour);
    ID = -1;
    moments = cv::moments(_contour);   
    area = moments.m00;
    xPos = moments.m10/area;
    yPos = moments.m01/area;
    centerPoint = cv::Point(moments.m10/area, moments.m01/area);
    prevXPos = 0;
    prevYPos = 0;
    matchFound = false;
    setLeadingEdge(boundingRect, productFlow);
    setTailingEdge(boundingRect, productFlow);
    color = TCAM_COLOR;
    rejected = false;
    gateHasOpened = false;
    gateHasClosed = false;
}


Product::~Product(void){ }

void Product::updateProduct(Product p)
{
    currentContour = p.currentContour;
    boundingRect = p.boundingRect;
    moments = p.getMoments();
    area = p.getArea();
    prevXPos = xPos;
    prevYPos = yPos;
    xPos = p.getXPos();
    yPos = p.getYPos();
    centerPoint = p.getCenterPoint();
    matchFound = true;
    leadingEdge = p.getLeadingEdge();
    tailingEdge = p.getTailingEdge();
}

int Product::getXPos()
{
    return centerPoint.x;
}

int Product::getYPos()
{
    return yPos;
}

cv::Point Product::getCenterPoint()
{
    return centerPoint;
}

int Product::getPrevXPos()
{
    return prevXPos;
}

int Product::getPrevYPos()
{
    return prevYPos;
}

int Product::getWeight()
{
    return weight;
}

int Product::getLength()
{
    return length;
}


bool Product::isRejected()
{
    return rejected;
}

void Product::setWeight(int val)
{
    weight = val;
    return;
}

void Product::setLength(int val)
{
    length = val;
    return;
}

void Product::setPrevXPos(int val)
{
    prevXPos = val;
    return;
}

void Product::setPrevYPos(int val)
{
    prevYPos = val;
    return;
}

int Product::getID()
{
    return ID;
}

size_t Product::getPOI()
{
    return POI;
}

double Product::getArea()
{
    return moments.m00;
}


cv::Moments Product::getMoments()
{
    return moments;
}

int Product::getLeadingEdge()
{
    return leadingEdge;
}
 
int Product::getTailingEdge()
{
    return tailingEdge;
}

cv::Scalar Product::getColor()
{
    return color;
}

void Product::setXPos(int x)
{
    xPos = x;
    return;
}

void Product::setYPos(int y)
{
    yPos = y;
    return;
}

void Product::setID(int productID)
{
    cout << "ID is: " << productID << endl;
    if(ID == -1)
    {
        ID = productID;
    }
    return;
}

void Product::setPOI(int POI)
{
    this->POI = POI;
    return;
}
void Product::setMoments(cv::Moments m)
{
    moments = m;
    return;
}

void Product::setRejectStatus(bool rejected)
{
    this->rejected = rejected;
    return;
}

void Product::setLeadingEdge(cv::Rect rect, std::string productFlow)
{
    if (productFlow == "Flow left to right")
    {
        leadingEdge = rect.br().x;    
    }
    else if (productFlow == "Flow top to bottom")
    {
       leadingEdge = rect.br().y; 
    }
    else if (productFlow == "Flow right to left")
    {
        leadingEdge = rect.tl().x;
    }
    else if (productFlow == "Flow bottom to top")
    {
        leadingEdge = rect.tl().y;
    }
    return;
}

void Product::setTailingEdge(cv::Rect rect, std::string productFlow)
{
    if (productFlow == "Flow left to right")
    {
        tailingEdge = rect.tl().x;    
    }
    else if (productFlow == "Flow top to bottom")
    {
       tailingEdge = rect.tl().y; 
    }
    else if (productFlow == "Flow right to left")
    {
        tailingEdge = rect.br().x;
    }
    else if (productFlow == "Flow bottom to top")
    {
        tailingEdge = rect.br().y;
    }
    
    return;
}

void Product::setColor(cv::Scalar color)
{
    this->color = color;
    return;
}

