/**
 @file Config.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */

#include "Frames.hpp"

Frames::Frames()
{
    int nRows = 1080;
    int nCols = nRows/2;
    Mat zeroMat = Mat(nRows,nCols, CV_64F, double(0));
    zeroMat.copyTo(cameraFeed);
    zeroMat.copyTo(cameraContrastFeed);
    zeroMat.copyTo(thresholdImage);    
    zeroMat.copyTo(areaOfInterestFeed);
    zeroMat.copyTo(productViewFeed);
    newAreaOfInterestImage = false;
    newImage = false;
    newThresholdImage = false;
}


Frames::~Frames()
{
    cameraFeed.release();
    cameraContrastFeed.release();
    thresholdImage.release();
    areaOfInterestFeed.release();
    productViewFeed.release();

}


