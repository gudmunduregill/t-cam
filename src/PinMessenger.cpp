/**
 @file PinMessenger.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
*/

#include <PinMessenger.hpp>

PinMessenger::PinMessenger()
{
    //Register pins
    _POI00.registerPin("POI00");
    _POI01.registerPin("POI01");
    _POI02.registerPin("POI02");
    _POI03.registerPin("POI03");
    _POI04.registerPin("POI04");
    _POI05.registerPin("POI05");
    _POI06.registerPin("POI06");
    _POI07.registerPin("POI07");
    _POI08.registerPin("POI08");
    _POI09.registerPin("POI09");
    _POI10.registerPin("POI10");
    _POI11.registerPin("POI11");
   // _isRecording.registerPin("isRecordingNow");
    if(USECAMERA)
    {
        _syncMessageSignal.registerPin("TCamSync");
        _syncMessageSlot.registerPin("syncMessageSlot");
        _syncMessageSlot.setNewstateCallback(this, &PinMessenger::onNewState);
    }
}

PinMessenger::~PinMessenger()
{

}

void PinMessenger:: onNewState(SyncSlot*, Sync::Item item)
{

    _currentSyncItem = item;
    SyncInfo s;
    s.pieceId = _currentSyncItem.getItemId();
    s.weight =  _currentSyncItem[ATT_WEIGHT].getValueAsInt();
    s.length =  _currentSyncItem[ATT_LENGTH].getValueAsInt();
    s.destination = _currentSyncItem[ATT_DESTINATION_GATE].getValueAsInt();
  
    items.push(s);
    cout << "Got sync item # " <<  item.getItemId() << endl;
    cout << item.toString() << endl;
}


void PinMessenger::messagePOI(size_t POIId, std::string action, int numberOfPOI)
{
    //TODO send messages to iomaster
    if(action == "open" && POIId < (size_t) numberOfPOI)
    {
        setPOI(POIId, true);
    }
    else if( action == "close" && POIId < (size_t) numberOfPOI)
    {
        setPOI(POIId, false);
    }
    //otherwise return
    return;
}

SyncInfo PinMessenger::getSyncInfo()
{
    SyncInfo s(-1,0,0);
    if(items.empty())
    {
        return s;
    }
     
    s = items.front();
    items.pop();

    return s;
}

int64_t PinMessenger::getPieceId()
{

    if(USECAMERA)
    {
       cout << "ID: " <<  _currentSyncItem.getItemId() << endl;
       cout << "Weight: " <<  _currentSyncItem[ATT_LENGTH].getValueAsDouble() << endl;
       cout << "Destination: " <<  _currentSyncItem[ATT_DESTINATION_GATE].getValueAsInt() << endl;

        return _currentSyncItem.getItemId();
    }

    return 12;
}

int64_t PinMessenger::getPieceWeight()
{
    if(USECAMERA)
    {
        return _currentSyncItem[ATT_WEIGHT].getValueAsInt();
    }

    return 300;
}

int64_t PinMessenger::getPieceLength()
{
    if(USECAMERA)
    {
        return _currentSyncItem[ATT_LENGTH].getValueAsInt();
    }

    return 300;
}


int64_t PinMessenger::getPieceDestination()
{
    if(USECAMERA)
    {
        return _currentSyncItem[ATT_DESTINATION_GATE].getValueAsInt();
    }

    return 1;
}

void PinMessenger::sendSyncMessage(Product p)
{
    Sync::Item item;

    item.setAttribute(ATT_PIECE_ID, to_string(p.getID())); 
    item.setAttribute(ATT_WEIGHT, Sync::Measurement(p.getWeight(), 0.001));
    item.setAttribute(ATT_DESTINATION_GATE, p.getPOI() + (int64_t)1);
    item.setAttribute(ATT_LENGTH,(int64_t) ( p.getLeadingEdge() - p.getTailingEdge() ));
  
    this->_syncMessageSignal.set(item);
}


void PinMessenger::setPOI(int64_t ID, bool val)
{
    if( ID == 0)
    {
        _POI00.set(val);
    }
    else if( ID == 1)
    {
        _POI01.set(val);
    }
    else if( ID == 2)
    {
        _POI02.set(val);
    }
    else if( ID == 3)
    {
        _POI03.set(val);
    }
    else if( ID == 4)
    {
        _POI04.set(val);
    }
    else if( ID == 5)
    {
        _POI05.set(val);
    }
    else if( ID == 6)
    {
        _POI06.set(val);
    }
    else if( ID == 7)
    {
        _POI07.set(val);
    }
    else if( ID == 8)
    {
        _POI08.set(val);
    }
    else if( ID == 9)
    {
        _POI09.set(val);
    }
    else if( ID == 10)
    {
        _POI10.set(val);
    }
    else if( ID == 11)
    {
        _POI11.set(val);
    }
    return;
}


