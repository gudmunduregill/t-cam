/**
 @file Config.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */

#include "Config.hpp"


Config::Config()
{
        //initialize configuration managers
    try
    {
        _TCamConfigReaderManager = std::tr1::shared_ptr<ConfigReaderInterface>(ConfigFactory::create(std::string("t-cam").c_str(), 0));
        _POIConfigReaderManager = std::tr1::shared_ptr<ConfigReaderInterface>(ConfigFactory::create(std::string("POI").c_str(), 0));
        _TCamConfigWriterManager = std::tr1::shared_ptr<ConfigWriterInterface>(ConfigWriterFactory::create(std::string("t-cam").c_str(), 0));
        _POIConfigWriterManager = std::tr1::shared_ptr<ConfigWriterInterface>(ConfigWriterFactory::create(std::string("POI").c_str(), 0));
        if(!_TCamConfigReaderManager )
        {
            throw Pluto::ErrorEx( "Unknown error when creating TCam config reader  manager" );
        }
        if(!_POIConfigReaderManager)
        {
            throw Pluto::ErrorEx( "Unknown error when creating POI config reader manager" );
        }
        if(!_TCamConfigWriterManager )
        {
            throw Pluto::ErrorEx( "Unknown error when creating TCam config writer  manager" );
        }
         if(!_POIConfigWriterManager)
        {
            throw Pluto::ErrorEx( "Unknown error when creating POI config writer manager" );
        }

    }
    catch (Pluto::ErrorEx& pex)
    {
        _plog(Pluto::Plog::Error, "Exception in initializing config: %s", pex.what());
         throw pex;
    }

    try
    {
        hMin  = _TCamConfigReaderManager->readInt("minHue");
        hMax  = _TCamConfigReaderManager->readInt("maxHue");
        sMin  = _TCamConfigReaderManager->readInt("minSaturation");
        sMax  = _TCamConfigReaderManager->readInt("maxSaturation");
        vMin  = _TCamConfigReaderManager->readInt("minValue");
        vMax  = _TCamConfigReaderManager->readInt("maxValue");
        AOIx0 = _TCamConfigReaderManager->readInt("AOIx0");
        AOIy0 = _TCamConfigReaderManager->readInt("AOIy0");
        AOIx1 = _TCamConfigReaderManager->readInt("AOIx1");
        AOIy1 = _TCamConfigReaderManager->readInt("AOIy1");
        entranceTolerance = _TCamConfigReaderManager->readInt("entranceTolerance");
        productFlow = _TCamConfigReaderManager->readString("productFlow");
        videoLength = _TCamConfigReaderManager->readInt("videoLength");
        record = _TCamConfigReaderManager->readInt("record");
        nPOI = _POIConfigReaderManager->readInt("nPOI");
    }
    catch(Pluto::ErrorEx& pex)
    {
        _plog(Pluto::Plog::Error, "Exception in reading TCamConfig: %s", pex.what());
        throw pex;
    }

    setPointsOfInterest();
    return;
}


Config::~Config()
{
 
}


//Get functions
int64_t Config::gethMin() 
{
    return _TCamConfigReaderManager->readInt("minHue");
}

int64_t Config::gethMax() 
{
    return _TCamConfigReaderManager->readInt("maxHue");
}

int64_t Config::getsMin() 
{    
    return _TCamConfigReaderManager->readInt("minSaturation");
}
int64_t Config::getsMax() 
{
    return _TCamConfigReaderManager->readInt("maxSaturation");
}

int64_t Config::getvMin() 
{
    return _TCamConfigReaderManager->readInt("minValue");
}

int64_t Config::getvMax() 
{ 
    return _TCamConfigReaderManager->readInt("maxValue");
}

int64_t Config::getAOIx0() 
{
    return _TCamConfigReaderManager->readInt("AOIx0");
}

int64_t Config::getAOIy0() 
{
    return _TCamConfigReaderManager->readInt("AOIy0");
}

int64_t Config::getAOIx1() 
{
    return _TCamConfigReaderManager->readInt("AOIx1");
}

int64_t Config::getAOIy1() 
{
    return _TCamConfigReaderManager->readInt("AOIy1");
}

int64_t Config::getEntranceTolerance() 
{
    return _TCamConfigReaderManager->readInt("entranceTolerance");
}

string  Config::getProductFlow() 
{
    return _TCamConfigReaderManager->readString("productFlow");
}

int32_t Config::getPOIXPos(size_t  POIId)
{

    updatePointsOfInterest();
    if(pointsOfInterest.size() > 0 && POIId < pointsOfInterest.size())
    {
        return pointsOfInterest.at(POIId).x;
    }
    else
    {
        return -1;
    }
}

int32_t Config::getPOIYPos(size_t POIId)
{
    updatePointsOfInterest();
    if(pointsOfInterest.size() > 0 && POIId < pointsOfInterest.size())
    {
        return pointsOfInterest.at(POIId).y;
    }
    else
    {
        return -1;
    }
}

int32_t Config::getNPOI()
{
    nPOI = _POIConfigReaderManager->readInt("nPOI");
    if (nPOI > MAX_N_POI)
    {
        return MAX_N_POI;
    }
    return nPOI;
}

vector<Point> Config::getPointsOfInterest()
{
    updatePointsOfInterest();
    return pointsOfInterest;
}

int64_t Config::getOpenBefore()
{
    return _POIConfigReaderManager->readInt("openBefore");
}

int64_t Config::getOpenAfter()
{
    return _POIConfigReaderManager->readInt("openAfter");
}

int64_t Config::getMaxDistanceAfterPOI()
{
     return _TCamConfigReaderManager->readInt("maxDistanceAfterPOI");
}

int64_t Config::getVideoLength()
{
    return _TCamConfigReaderManager->readInt("videoLength");
}

int64_t Config::getPOIAbsolute(size_t POIId)
{
    if (this->getProductFlow() == "Flow left to right")
    {
        return this->getPOIXPos(POIId);
    }
    else if (this->getProductFlow() == "Flow top to bottom")
    {
        return this->getPOIYPos(POIId);
    }
    else if (this->getProductFlow() == "Flow right to left")
    {
        return this->getPOIXPos(POIId);
    }
    else if (this->getProductFlow() == "Flow bottom to top")
    {
        return this->getPOIYPos(POIId);
    }

    return 0;
}




bool Config::recordVideo()
{
    return _TCamConfigReaderManager->readInt("record") == 1;
}


//Set functions
void Config::sethMin(int64_t val) 
{
    hMin = val;
    _TCamConfigWriterManager->write("minHue", val);
    return;
}



void Config::sethMax(int64_t val) 
{ 
    hMax = val;
    _TCamConfigWriterManager->write("maxHue", val);
    return;
}

void Config::setsMin(int64_t val) 
{
    sMin = val;
    _TCamConfigWriterManager->write("minSaturation", val);
    return;
}

void Config::setsMax(int64_t val) 
{
    sMax = val; 
    _TCamConfigWriterManager->write("maxSaturation", val);
    return;
}

void Config::setvMin(int64_t val) 
{
    vMin = val; 
    _TCamConfigWriterManager->write("minValue", val);
    return;
}

void Config::setvMax(int64_t val) 
{
    vMax = val; 
    _TCamConfigWriterManager->write("maxValue", val);
    return;
}

void Config::setProductFlow(char* val)  
{
    productFlow = val;
    _TCamConfigWriterManager->write("productFlow", val);
    return;
}

void Config::setEntranceTolerance(int64_t val)
{
    entranceTolerance = val;
    _TCamConfigWriterManager->write("entranceTolerance", val);
    return;
}

void Config::setAOIx0(int64_t val) 
{
    try
    {
        _TCamConfigWriterManager->write("AOIx0", val);
    }
    catch(Pluto::ErrorEx& pex)
    {
        _plog(Pluto::Plog::Error, "Exception in writing TCamConfig AOIx0: %s", pex.what());
        throw pex;
    }
}

void Config::setAOIy0(int64_t val) 
{
    try
    {
        _TCamConfigWriterManager->write("AOIy0", val);
    }
    catch(Pluto::ErrorEx& pex)
    {
        _plog(Pluto::Plog::Error, "Exception in writing TCamConfig AOIy0: %s", pex.what());
        throw pex;
    }
}

void Config::setAOIx1(int64_t val) 
{
    try
    {
        _TCamConfigWriterManager->write("AOIx1", val);
    }
    catch(Pluto::ErrorEx& pex)
    {
        _plog(Pluto::Plog::Error, "Exception in writing TCamConfig AOIx1: %s", pex.what());
        throw pex;
    }
}

void Config::setAOIy1(int64_t val) 
{
    try
    {
        _TCamConfigWriterManager->write("AOIy1", val);
    }
    catch(Pluto::ErrorEx& pex)
    {
        _plog(Pluto::Plog::Error, "Exception in writing TCamConfig AOIy1: %s", pex.what());
        throw pex;
    }
}

//Private methods
void Config::setPointsOfInterest()
{
    //nPOI = _POIConfigReaderManager->readInt("nPOI");
    //TODO remove and use values from config file 
    for (int i = 0; i < getNPOI(); i++)
    {
        std::string s1 = "POIx" + to_string(i);
        std::string s2 = "POIy" + to_string(i);
        int x = _POIConfigReaderManager->readInt(s1.c_str());
        int y = _POIConfigReaderManager->readInt(s2.c_str());
        Point p(x,y);
        pointsOfInterest.push_back(p);
    }
}

void Config::updatePointsOfInterest()
{

    size_t n = getNPOI();
    for (size_t i = 0; i < pointsOfInterest.size(); i++)
    {
        std::string s1 = "POIx" + to_string(i);
        std::string s2 = "POIy" + to_string(i);
        int x = _POIConfigReaderManager->readInt(s1.c_str());
        int y = _POIConfigReaderManager->readInt(s2.c_str());
        Point p(x,y);
        pointsOfInterest.at(i) = p;
    }

    
    size_t currentSize = pointsOfInterest.size();
    if(currentSize < n)
    {
        for (size_t i = currentSize ; i < n; i++)
        {
            std::string s1 = "POIx" + to_string(i);
            std::string s2 = "POIy" + to_string(i);
            int x = _POIConfigReaderManager->readInt(s1.c_str());
            int y = _POIConfigReaderManager->readInt(s2.c_str());
            Point p(x,y);
            pointsOfInterest.push_back(p);

        }
        cout << "add" << endl; 
    }
    else if( currentSize > n)
    {
        pointsOfInterest.erase( pointsOfInterest.begin() + n);
    }

}

void Config::setPOI(int ID, int x, int y)
{
    //nPOI = _POIConfigReaderManager->readInt("nPOI");
    if(ID >= getNPOI())
    {
        return;
    }
    string pointX = "POIx" + to_string(ID);
    string pointY = "POIy" + to_string(ID);

    _POIConfigWriterManager->write(pointX.c_str(), x);
    _POIConfigWriterManager->write(pointY.c_str(), y);
    
    pointsOfInterest.at(ID).x = x;
    pointsOfInterest.at(ID).y = y;
    return;
}

void Config::stopRecord() 
{
    record = 0;
    _TCamConfigWriterManager->write("record", 0);
//    _isRecordingSignal.set(false);
    return;
}

/*
void onNewstateisRecording(DIOSlot* slot, bool value)
{
    return;
}
*/
