/**
 @file ProductionViewController.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */

#include "ProductionViewController.hpp"
#include <GlobalVar.hpp>

ProductionViewController::ProductionViewController() 
{
    VideoCapture capture;

    string s = "";// "/home/pluto/t-cam/img/1.png";
    for(int i = 0; i < MAX_N_POI; i++)
    {
       /* capture.open(s);
        if(!capture.isOpened())
        {
            exit(0)
        }
        capture >> numbers[i];
        capture.release();
        */
        
        s = "/home/pluto/t-cam/img/" + to_string(i) + ".png";
        cout << s << endl;
        numbers.push_back(imread(s, CV_LOAD_IMAGE_COLOR));
        if(! numbers.at(i).data )                              // Check for invalid input
        {
            cout <<  "Could not open or find the image for POI numbering" << std::endl ;
            exit(0);
        }
        s = ""; 
    }
 

}
ProductionViewController::~ProductionViewController() 
{
    for(int i = 0; i < numbers.size(); i++)
    {
        numbers.at(i) = cv::Mat();
    }

}


void ProductionViewController::productionView(Config* config, Frames* frames, vector<Product> products)
{

    frames->productViewFeed.release();
    frames->cameraFeed.copyTo(frames->productViewFeed);           
    this->drawObject(products, 
                                          frames->productViewFeed, 
                                          TCAM_COLOR, 
                                          config->getAOIx0(), 
                                          config->getAOIy0());
    this->drawPOI(config, frames); 
    return;
}



void ProductionViewController::drawPOI(Config* config, Frames* frames) 
{
    int i = 0;
    //int numPoints = _TCam->pointsOfInterest.size() - 1;
    //int offset = 0;
    //string text;
    //double fontSize = 4;
    //int thickness = 3;
    //Point p;

    for (auto &currentPoint : config->getPointsOfInterest())
    {
        //text = to_string(i++);
        //insertText(frames->productViewFeed, currentPoint, text, fontSize, thickness, TCAM_COLOR);
        if(frames->productViewFeed.cols < numbers[i].cols || frames->productViewFeed.rows < numbers[i].rows )
        {
            cout << "POI image is to large for camera feed, skipping point and continuing" << endl;
            continue;
        }

        int x =(int) currentPoint.x - (int) numbers[i].cols/2;
        int y =(int) currentPoint.y - (int) numbers[i].rows/2;

        //Make sure POI image is inside camera frame
        if( x > (int)frames->productViewFeed.cols - (int)numbers[i].cols )
        {
            x = (int)frames->productViewFeed.cols -(int) numbers[i].cols;
        }
        else if ( x < (int)numbers[i].cols/2)
        {
            x = 0;
            
        }
        if( y > (int)frames->productViewFeed.rows - (int)numbers[i].rows )
        {
            y = (int)frames->productViewFeed.rows - (int)numbers[i].rows;
        }

        if ( y < (int)numbers[i].cols/2)
        {
            y = 0;
        }


        numbers[i].copyTo( frames->productViewFeed(cv::Rect(x, y, (int)numbers[i].cols, (int)numbers[i].rows)));
        i++;
    }
    return;
}

void ProductionViewController::insertText(Mat &img, Point p, string text, int fontSize, int thickness, Scalar color)
{
           
    int bottom = 0;

    cv::Point pText(p.x, p.y * 1.02);

    Size textSize = cv::getTextSize(text, FONT, fontSize, thickness, &bottom);
    rectangle(img, p + cv::Point(0, bottom ), p + cv::Point(textSize.width, - textSize.height), SCALAR_BLUE, CV_FILLED);
    putText(img, text, pText, FONT, fontSize, color, thickness, 8);

    return;
}

void ProductionViewController::insertTextProduct(Mat &img, Point p, string text, int fontSize, int thickness, Scalar color)
{

    int bottom = 0;


    Size textSize = cv::getTextSize(text, FONT, fontSize, thickness, &bottom);
    rectangle(img, p + cv::Point(0, bottom ), p + cv::Point(textSize.width, - textSize.height), SCALAR_BLUE, CV_FILLED);
    putText(img, text, p, FONT, fontSize, color, thickness, 8);

    return;
}

void ProductionViewController::drawPOI(vector<Point> points, Mat &frame, Scalar color, int AOIx, int AOIy0, int AOIy1) 
{
    int i = 0;
    int offset = 200; //(AOIy1 - AOIy0) / 2;
    Point p;

    for (auto &currentPoint : points)
    {
        if( i % 2 == 0)
        {
            p = Point(currentPoint.x + AOIx, currentPoint.y + AOIy0 - offset);
        }
        else
        {
            p = Point(currentPoint.x + AOIx, currentPoint.y + AOIy1 + offset);
        }

        putText(frame, to_string(i++), p, 1, 6, color,4);
    }
    return;
}




void ProductionViewController::drawObject(vector<Product> products, Mat &frame, Scalar color, int AOIx, int AOIy)
{
    if(products.size() == 0)
    {
        return;
    }

    int distToEdge = 25;
    int fontSize = 1;
    int thickness = 2;
    int lineSpace = 25 * fontSize;
 

    for (size_t i = 0; i < products.size(); i++)
    {
        int x = products.at(i).getXPos() + AOIx;
        int y = products.at(i).getYPos() + AOIy;
       
        cv::Point topLeft = products.at(i).boundingRect.tl();
        cv::Point topRight = products.at(i).boundingRect.tl();
        cv::Point bottomRight = products.at(i).boundingRect.br();

        topLeft.x += AOIx;
        topLeft.y += AOIy;
        bottomRight.x += AOIx;
        bottomRight.y += AOIy;
        topRight.x = bottomRight.x;
        topRight.y = topRight.y + AOIy + fontSize; 
       

        color = products.at(i).getColor(); 

        //Draw center circle
        circle(frame, Point(x,y), 20, color,2);

        rectangle(frame, topLeft, bottomRight, color, 2, 8, 0 );

        //Draw a cross trough center circle
        if(y - distToEdge > 0)
        {
            line(frame, Point(x,y), Point(x,y - distToEdge), color,2);
        }
        else 
        {
            line(frame, Point(x,y), Point(x,0), color,2);
        }
        if(y + distToEdge < frame.rows)
        {
            line(frame, Point(x,y), Point(x,y + distToEdge), color,2);
        }
        else 
        {
            line(frame, Point(x,y), Point(x, frame.rows), color,2);
        }
        if(x - distToEdge > 0)
        {
            line(frame, Point(x,y), Point(x - distToEdge,y), color,2);
        }
        else 
        {
            line(frame, Point(x,y), Point(0,y), color,2);
        }
        if(x + distToEdge < frame.cols)
        {
            line(frame, Point(x, y), Point(x + distToEdge, y), color,2);
        }
        else 
        {
            line(frame, Point(x, y), Point(frame.cols, y), color,2);
        }
        insertTextProduct(frame, topRight, to_string(products.at(i).getID()), fontSize, thickness, products.at(i).getColor() );
        //add linespace
        topRight.y = topRight.y + lineSpace;
        insertTextProduct(frame, topRight, "POI: " + to_string(products.at(i).getPOI()), fontSize, thickness, products.at(i).getColor());
        //add linespace
        topRight.y = topRight.y + lineSpace;
        insertTextProduct(frame, topRight, "weight: " + to_string( products.at(i).getWeight() ), fontSize, thickness, products.at(i).getColor());
    }
    return;
}



double ProductionViewController::distance(Point p1, Point p2)
{
    int x = abs(p2.x - p1.x);
    int y = abs(p2.y - p1.y);

    return sqrt(pow(x, 2) + pow(y, 2));
}


