#include <ProductTracker.hpp>


ProductTracker::ProductTracker()
{
    firstImage = true;
    pinMessenger = new PinMessenger();
    productID = 0;
    rejectedProducts = new vector<Product>;
}

ProductTracker::~ProductTracker()
{
    delete pinMessenger;
    delete rejectedProducts;
}


//! A helper function, Displays a black and white image
/*!
  \param imageSize 
  \param contours vector with all contour points 
  \param strImageName, display name for image
*/
void drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours)
{

    cv::Mat image(imageSize, CV_8UC3, SCALAR_BLACK);

    cv::drawContours(image, contours, -1, SCALAR_WHITE, -1);

    //Fill holes in threshold image
    cv::Mat edgesFill = image.clone();

    cv::floodFill(edgesFill, cv::Point(0,0), SCALAR_WHITE); // CV_RGB(255,255,255));
    bitwise_not(edgesFill, edgesFill);

    image = (edgesFill | image);

    //cv::imshow(strImageName, image);
}


//! A helper function, Takes a thresholded image and 
//! morphs grop of small objects together
/*!
  \param threshold image to be morphed
*/
void ProductTracker::morphImage(Mat &threshold){

    //Use a 3x3 pixel to erode the image
    Mat erodeElement = getStructuringElement( cv::MORPH_RECT,Size(3, 3));
    //dilate with larger element so make sure object is nicely visible 8x8
    Mat dilateElement = getStructuringElement( cv::MORPH_RECT,Size(8, 8));

    //repete twice
    erode(threshold,threshold,erodeElement);

    dilate(threshold,threshold,dilateElement);

    return;
}


vector<Product> ProductTracker::findObjects(Mat threshold, Mat &cameraFeed, string productFlow){
        
    //Create a vector of products
    vector<Product> currentProducts;

    Mat temp = threshold.clone();

    //these two vectors for output of findContours
    vector<Vec4i> hierarchy;
    vector< vector<Point> > contours;
    //find contours of filtered image using openCV findContours function
    if( !temp.empty() )
    {  
        cv::findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    }
    else
    {
        return currentProducts;
    }

    double refArea = 0;
    bool objectFound = false;


    std::vector<std::vector<cv::Point> > convexHulls(contours.size());
    //Create a convex hull for all items
    for (unsigned int i = 0; i < contours.size(); i++) {
        cv::convexHull(contours[i], convexHulls[i]);
    }

    drawAndShowContours(cameraFeed.size(), convexHulls);


    if (hierarchy.size() > 0) 
    {
        int numObjects = hierarchy.size();
        //if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
        if(numObjects<MAX_NUM_PRODUCTS)
        {
            for (int index = 0; index >= 0; index = hierarchy[index][0]) 
            {
                Moments moment = moments((cv::Mat)convexHulls[index]);
                double area = moment.m00;

                if(area > MIN_PRODUCT_SIZE && area < MAX_PRODUCT_SIZE && area > refArea)
                {
                    Product product(contours[index], productFlow);

                    currentProducts.push_back(product);
                                         
                    objectFound = true;
                    //refArea = area;
                }
                else 
                {
                    objectFound = false;
                }
            }
            if(objectFound == false)
            {
                putText(cameraFeed,"To much noice look into contrast settings", Point(0,50), FONT, 2, SCALAR_RED, 2);
            }
        }
    }
    return currentProducts;
}


void ProductTracker::checkPointsOfInterest(Config* config, vector<Product>* products )
{

    /*
    //For performance test uncomment to get piece dx between frames
    if(products->size() > 0)
    {
        std::ofstream outfile;
        outfile.open("Beltspeed0.7.txt", std::ios_base::app); 

        cout << "Product ID: " << products->at(0).getID() << endl;
        //cout << "Number of products: " << products->size() << endl;
        cout << "Differance in x pos  " << abs( products->at(0).getXPos() - products->at(0).getPrevXPos())<< endl;
        cout << "piece length" << abs(products->at(0).getLeadingEdge() - products->at(0).getTailingEdge())<< endl;
        //write data to file for T-CAM benchmark
        outfile << "Product ID: " << products->at(0).getID() << endl;
        //cout << "Number of products: " << products->size() << endl;
        outfile << "Differance in x pos  " << abs( products->at(0).getXPos() - products->at(0).getPrevXPos())<< endl;
        outfile << "piece length" << abs( products->at(0).getLeadingEdge() - products->at(0).getTailingEdge())<< endl;

        outfile.close();
    }
    */
    
    //TODO handle if product is not caught by module and continues to be logged
    if (config->getProductFlow() == "Flow left to right")
    {
        //Loop through all products and compare to POI
        for (size_t i = 0 ; i < products->size(); i++)
        {
            //if product has reached its final destination or end of camera frame
            if(products->at(i).getLeadingEdge() + config->getOpenBefore() > config->getPOIXPos(products->at(i).getPOI()) && products->at(i).gateHasOpened == false)
            {
                pinMessenger->messagePOI(products->at(i).getPOI(), "open", config->getNPOI()); 
                products->at(i).setColor(SCALAR_RED);
                products->at(i).gateHasOpened = true;

            }

            if(products->at(i).getTailingEdge() - config->getOpenAfter() > config->getPOIXPos(products->at(i).getPOI())  && products->at(i).gateHasClosed == false ) 
            {
                pinMessenger->messagePOI(products->at(i).getPOI(), "close", config->getNPOI());
                products->at(i).setRejectStatus(true);
                products->at(i).setColor(SCALAR_BLUE);
                products->at(i).gateHasClosed = true;
            }
        }
    }
    else if (config->getProductFlow() == "Flow top to bottom")
    {
        //Loop through all products and compare to POI
        for (size_t i = 0 ; i < products->size(); i++)
        {

            if(products->at(i).getLeadingEdge() + config->getOpenBefore()  > config->getPOIYPos(products->at(i).getPOI())   && products->at(i).gateHasClosed == false)
            {
                pinMessenger->messagePOI(products->at(i).getPOI(), "open", config->getNPOI()); 
                products->at(i).setColor(SCALAR_RED);
                products->at(i).gateHasOpened = true;
            }

            if(products->at(i).getTailingEdge() - config->getOpenAfter() > config->getPOIYPos(products->at(i).getPOI()) && products->at(i).gateHasClosed == false)
            {
                pinMessenger->messagePOI(products->at(i).getPOI(), "close", config->getNPOI());
                products->at(i).setRejectStatus(true);
                products->at(i).setColor(SCALAR_BLUE);
                products->at(i).gateHasClosed = true;
            }
        }
    }
    else if (config->getProductFlow() == "Flow right to left")
    {
        //Loop through all products and compare to POI
        for (size_t i = 0 ; i < products->size(); i++)
        {

            if(products->at(i).getLeadingEdge() - config->getOpenBefore() < config->getPOIXPos(products->at(i).getPOI())  && products->at(i).gateHasClosed == false )
            {
                pinMessenger->messagePOI(products->at(i).getPOI(), "open", config->getNPOI()); 
                products->at(i).setColor(SCALAR_RED);
                products->at(i).gateHasOpened = true;
            }
 
            if(products->at(i).getTailingEdge() + config->getOpenAfter() < config->getPOIXPos(products->at(i).getPOI()) && products->at(i).gateHasClosed == false)
            {
                pinMessenger->messagePOI(products->at(i).getPOI(), "close", config->getNPOI());
                products->at(i).setRejectStatus(true);
                products->at(i).setColor(SCALAR_BLUE);
                products->at(i).gateHasClosed = true;
            }
        }
    }
    else if (config->getProductFlow() == "Flow bottom to top")
    {
         //Loop through all products and compare to POI
        for (size_t i = 0 ; i < products->size(); i++)
        {


            if(products->at(i).getLeadingEdge() - config->getOpenBefore() < config->getPOIYPos(products->at(i).getPOI())  && products->at(i).gateHasClosed == false)
            {
                pinMessenger->messagePOI(products->at(i).getPOI(), "open", config->getNPOI()); 
                products->at(i).setColor(SCALAR_RED);
                products->at(i).gateHasOpened = true;
            }

            if(products->at(i).getTailingEdge() + config->getOpenAfter() < config->getPOIYPos(products->at(i).getPOI()) && products->at(i).gateHasClosed == false)
            {
                pinMessenger->messagePOI(products->at(i).getPOI(), "close", config->getNPOI());
                products->at(i).setRejectStatus(true);
                products->at(i).setColor(SCALAR_BLUE);
                products->at(i).gateHasClosed = true;
            }
        }
    }
    return;
}


void ProductTracker::updateTrackedProducts(vector<Product>* products, vector<Product>* currentImageProducts, Config* config)
{

    //Initialize all tracked products matching status
    for (size_t i = 0; i < products->size(); i++ ) 
    {
        products->at(i).matchFound = false;
    }    
    //now check rejected products
    if(rejectedProducts->size() > 0)
    {

        //Initialize all tracked products matching status
        for (size_t i = 0; i < rejectedProducts->size(); i++ )
        {
            rejectedProducts->at(i).matchFound = false;
        }

        for (size_t i = 0 ; i < currentImageProducts->size(); i++)
        {
                //Loop through rejected products
                for (size_t j = 0; j < rejectedProducts->size(); j++)
                {

                //Loop through rejected products
                    if(rejectedProducts->at(j).matchFound == false)
                    {
 
                        if( pointPolygonTest(rejectedProducts->at(j).currentContour,  currentImageProducts->at(i).getCenterPoint(), false) > 0)
                        {
                            //cout << "Updating rejected product with ID: " << rejectedProducts->at(j).getID() << endl;
                            currentImageProducts->at(i).matchFound = true;
                            rejectedProducts->at(j).updateProduct(currentImageProducts->at(i));
                        }
                    }
                }
        }
    }


    //Loop though all products in new image and match to 
    //already tracked products
    for (size_t i = 0 ; i < currentImageProducts->size(); i++)
    {
        //Loop through tracked products
        for (size_t j = 0; j < products->size(); j++)
        {
            if(products->at(j).matchFound == false)
            {
                if( pointPolygonTest(products->at(j).currentContour,  currentImageProducts->at(i).getCenterPoint(), false) > 0)
                {
                    currentImageProducts->at(i).matchFound = true;
                    products->at(j).updateProduct(currentImageProducts->at(i));
                }
            }
        }
    } 
    //Now loop throught found products again if there is still a product with matchFound flag == false
    //it must be a new product or a lost product
    //for (auto &currentImageProduct : currentImageProducts)
    //{
    for (size_t i = 0 ; i < currentImageProducts->size(); i++)
    {
        //Found new product 
        //TODO get data from syncmessage here
        if(currentImageProducts->at(i).matchFound == false)
        {
            if(isAtEntrance(currentImageProducts->at(i), config))
            {
                cout << "Adding new product" << endl;
                if(USECAMERA)
                {
                    SyncInfo temp = pinMessenger->getSyncInfo();
                    if(temp.pieceId == -1)
                    {
                        cout << "Lost sync message ignoring product" << endl;
                        continue;
                    }
                    else
                    {
                        currentImageProducts->at(i).setID(temp.pieceId); 
                        currentImageProducts->at(i).setPOI(temp.destination - 1);
                        currentImageProducts->at(i).setWeight(temp.weight);
                        currentImageProducts->at(i).setLength(temp.length);
                    }
                }
                else if(USEFILE || USEWEBCAM )
                {

                    cout << "Adding new product" << endl;
                    currentImageProducts->at(i).setID(productID++);
                    //Alternate product POI between 2 and 4
                    if((productID % 2) == 0)
                    {
                        currentImageProducts->at(i).setPOI(2);
                    }
                    else
                    {
                         currentImageProducts->at(i).setPOI(4);
                    }
                    currentImageProducts->at(i).setWeight(pinMessenger->getPieceWeight());
                }

                products->push_back(currentImageProducts->at(i));
                
            }
        } 
    }

    handleNotRejectedPieces(config, products);
    return;
}


bool ProductTracker::isAtEntrance(Product product, Config* config) // void* ctx)
{
    int64_t tolerance = config->getEntranceTolerance();

    if (config->getProductFlow() == "Flow left to right")
    {
        if( (product.getXPos() < config->getAOIx0() + tolerance) && (product.getXPos() > config->getAOIx0()) )
        {
            return true;
        }
    }
    else if (config->getProductFlow() == "Flow top to bottom")
    {
        if( (product.getYPos() < config->getAOIy0() + tolerance) && (product.getYPos() > config->getAOIy0()) )
        {
            return true;
        }
    }
    else if (config->getProductFlow() == "Flow right to left")
    {
        if( (product.getXPos() > config->getAOIx1() - tolerance) && (product.getXPos() < config->getAOIx1()) )
        {
            return true;
        }
    }
    else if (config->getProductFlow() == "Flow bottom to top")
    {
        if( (product.getYPos() > config->getAOIy1() - tolerance) && (product.getYPos() < config->getAOIy1()) )
        {
            return true;
        }
    }

    return false;
}

bool ProductTracker::addProduct(vector<Product>* products, Product product)
{
    if( product.getID() > 0)
    {
        products->push_back(product);
        return true;
    }
    else
    {
        return false;
    }

}

bool ProductTracker::removeProductByID(vector<Product>* products, int productID)
{
    if(productID != -1 && products->size() == 1)
    {
        products->pop_back();
    }

    for(size_t i = 0; i < products->size(); i++)
    {
        if(products->at(i).getID() == productID)
        {
            products->erase (products->begin() + i);
            return true;
        }
    }

    return false;
}

void ProductTracker::handleNotRejectedPieces(Config* config, vector<Product>* products)
{

    if(products->size() <= 0 && rejectedProducts->size() == 0)
    {
        return;
    }

    for(size_t j = 0; j < products->size(); j++)
    {
        if( products->at(j).gateHasClosed == true)
        {
            cout << "Removing product from tracking list with id: " << products->at(j).getID() << endl;
            rejectedProducts->push_back(products->at(j));
            removeProductByID(products, products->at(j).getID());
        }
    }

    for(size_t j = 0; j < rejectedProducts->size(); j++)
    {
        if (config->getProductFlow() == "Flow left to right")
        {
                if( rejectedProducts->at(j).getTailingEdge() > config->getPOIAbsolute( rejectedProducts->at(j).getPOI() ) + config->getMaxDistanceAfterPOI() )
                {
                    cout << "Product has exceeded gate sending sync message" << endl;
                    try
                    {
                       pinMessenger->sendSyncMessage(rejectedProducts->at(j));
                    }
                    catch(const exception& e)
                    {
                        cout << "Pin is not connected" << endl;
                       //TODO throw exception
                    }
                    removeProductByID(rejectedProducts, rejectedProducts->at(j).getID());
                }
        }
        else if (config->getProductFlow() == "Flow top to bottom")
        {
               if(rejectedProducts->at(j).getTailingEdge() > config->getPOIAbsolute( rejectedProducts->at(j).getPOI() ) + config->getMaxDistanceAfterPOI())
                {
                    cout << "Product has exceeded gate sending sync message" << endl;
                    try
                    { 
                        pinMessenger->sendSyncMessage(rejectedProducts->at(j));
                    }
                    catch(const std::exception& e)
                    {
                        cout << "Pin is not connected" << endl;
                        //TODO throw exception
                    } 
                    removeProductByID(rejectedProducts, rejectedProducts->at(j).getID());
                }
        }
        else if (config->getProductFlow() == "Flow right to left")
        {
 
                if( rejectedProducts->at(j).getTailingEdge() < config->getPOIAbsolute( rejectedProducts->at(j).getPOI() ) - config->getMaxDistanceAfterPOI())
                { 
                    cout << "Product has exceeded gate sending sync message" << endl;
                    try
                    {
                        pinMessenger->sendSyncMessage(rejectedProducts->at(j));
                    }
                    catch(const std::exception& e)
                    {
                        cout << "Pin is not connected" << endl;
                        //TODO throw exception
                    }
                    removeProductByID(rejectedProducts, rejectedProducts->at(j).getID());
                }
        }
        else if (config->getProductFlow() == "Flow bottom to top")
        {
                if( rejectedProducts->at(j).getTailingEdge() < config->getPOIAbsolute( rejectedProducts->at(j).getPOI() ) - config->getMaxDistanceAfterPOI())
                { 
                    cout << "Product has exceeded gate sending sync message" << endl;
                    try
                    {
                        pinMessenger->sendSyncMessage(rejectedProducts->at(j));
                    }
                    catch(const std::exception& e) 
                    {
                        cout << "Pin is not connected" << endl;
                        //TODO throw exception
                    }
                    removeProductByID(rejectedProducts, rejectedProducts->at(j).getID());
                }
  
        }
    } 
    return;   
}

