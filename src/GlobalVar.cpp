#include <GlobalVar.hpp>


cv::Mat BASE_VIEW = cv::Mat::zeros(cv::Size(1, 49), CV_64FC1);

const cv::Scalar SCALAR_BLACK = cv::Scalar(0.0, 0.0, 0.0);
const cv::Scalar SCALAR_WHITE = cv::Scalar(255.0, 255.0, 255.0);
const cv::Scalar SCALAR_RED = cv::Scalar(0.0, 0.0, 255.0);
const cv::Scalar TCAM_COLOR = cv::Scalar(0, 140, 255);
const cv::Scalar SCALAR_BLUE = cv::Scalar(60.0, 10.0, 0.0);
const int FONT =  cv::FONT_HERSHEY_DUPLEX;
const int MAX_N_POI = 12;
const bool USECAMERA = false;
const bool USEFILE = true;
const bool USEWEBCAM = false;
const std::string VIDEOFILEPATH_A = "/var/lighttpd/html/t-cam-video/TCAMrecording1.ogg";
const std::string VIDEOFILEPATH_B = "/var/lighttpd/html/t-cam-video/TCAMrecording2.ogg";
const std::string VIDEOFILEPATH = "/var/lighttpd/html/t-cam-video/";
const int MAX_N_VIDEOFILES = 10;

