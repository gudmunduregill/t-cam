/**
 @file CameraController.cpp

 @author Your-name-here <your.email.here@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */

#include "CamServer.hpp"


CamServer::CamServer() { }
CamServer::~CamServer() { }

ClientHandler* CamServer::newClient()
{
    ClientHandler* foo = new ClientHandler();
    _clients.push_back(foo);
    return foo;
}

bool CamServer::peerAdded(int s, const std::string& str)
{
    std::cout << "WS: client added: " << str << std::endl;
    return WebSocketServer::peerAdded(s, str);
}

bool CamServer::peerRemoved(int s, const std::string& str)
{
    std::cout << "WS: client removed: " << str << std::endl;
    return WebSocketServer::peerRemoved(s, str);
}


