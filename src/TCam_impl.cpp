/**
 @file TCamimpl.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
*/


#include "TCam.h"


using namespace std;
using namespace cv;
using namespace Pluto;
using namespace Pluto::Pins;

#include <ConfigFactory.h>
#include <ConfigWriterFactory.h>

TCam::TCam(std::string appName M_UU):
    MarelCppApp(appName)
{
    _frames = new Frames();
    _config = new Config();
    products = new vector<Product>;
    pointsOfInterest = new vector<Point>;
    sendVideoFile = true;
}


TCam::~TCam()
{
    //close threads
    osa_thread_request_termination(&TCamLogicThread);
    osa_thread_join(&TCamLogicThread, &TCamLogicThread_return);
    
    delete _config;
    delete _frames;
    delete products;
    delete pointsOfInterest;
}

eloop_cb_error_t timer_timeout(eloop_timer_t t M_UU, void* context)
{
    TCam* oc = (TCam*)context;
    oc->onTimeout();
    return eloop_cb_ok;
}


int32_t
TCam::initialize(int32_t argc M_UU, char** argv M_UU)
{
    //BaseViewController _BaseViewController;
    AdjustContrastViewController _AdjustContrastViewController;
    ProductionViewController _ProductionViewController;

    //Run all controllers
    cout << "Run camera live view thread.." << endl;
    //osa_thread_create(&baseViewThread, "CameraLiveView", _BaseViewController.liveCameraView, this, 0, 10000000);
      
    osa_thread_create(&TCamLogicThread, "CameraLiveView", &TCam::TCamLogic, this, 0, 10000000);      
    

    //Set all callbacks
    //Callbacks for adjust contrast view
    _hMin.setNewstateCallback(this, &TCam::onNewstatehMin);
    _hMax.setNewstateCallback(this, &TCam::onNewstatehMax);
    _sMin.setNewstateCallback(this, &TCam::onNewstatesMin);
    _sMax.setNewstateCallback(this, &TCam::onNewstatesMax);
    _vMin.setNewstateCallback(this, &TCam::onNewstatevMin);
    _vMax.setNewstateCallback(this, &TCam::onNewstatevMax);
    _vMax.setNewstateCallback(this, &TCam::onNewstatevMax);
    _productFlow.setNewstateCallback(this, &TCam::onNewstateproductFlow);
    
 
    //Initialize Pluto pins
    _tCamIP.registerPin("tCamIP");
    _hMin.registerPin("hMin");
    _hMax.registerPin("hMax");    
    _sMin.registerPin("sMin");    
    _sMax.registerPin("sMax");    
    _vMin.registerPin("vMin");    
    _vMax.registerPin("vMax");    
    _productFlow.registerPin("productFlow");
   
    //Create a mutex to use between threads
    mutex = osa_mutex_create();
    newImage = false;
    newAreaOfInterestImage = false;
    newThresholdImage = false;

    osa_mutex_lock(mutex, 0);
    cout << "Initialize Camserver" << endl;
     if(!_CamServer.init("8183", "vision_tracking"))
    {
        osa_mutex_unlock(mutex);
        cout << "Camserver init process aborted" << endl;
        abort();
    }
    //start eloop function this function sends uses eloop timer and the timer_timeout 
    //function to send data to the view 
    cout << "Call eloop" << endl;
    eloop_timer_new(eloop_default_loop(), 200, 1, timer_timeout, 0, this, &_timer);

    //Initialize image compression parameters, select either png or jpg
    //compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(20);

    osa_mutex_unlock(mutex);
    //set TCam server ip-address
    _tCamIP.set("10.100.11.88:8183");

    //initialize all image containers
    string imageName("img/TCAM-LOGO.png");
    cameraFeed = imread(imageName.c_str(), IMREAD_COLOR);
    cameraContrastFeed = imread(imageName.c_str(), CV_LOAD_IMAGE_GRAYSCALE);
    areaOfInterestFeed = imread(imageName.c_str(), IMREAD_COLOR);
    productViewFeed = imread(imageName.c_str(), IMREAD_COLOR);

    initComplete = true;
    cout << "Init complete" << endl;
    return 0;
}


void
TCam::deinitialize()
{
    //Stop services
    eloop_timer_del(eloop_default_loop(), _timer);
    _CamServer.deinit();
    _isStopped = true;
}


void
TCam::timeout()
{
    cout << endl << __PRETTY_FUNCTION__ << endl;

    //TODO Implement your timeout functionality here
}


int32_t
TCam::onTmDead(mqs_streamid_t sid M_UU, const char* ep M_UU)
{
    cout << endl << __PRETTY_FUNCTION__ << endl;

    //TODO Implement your ticker master exit functionality here
    //Warning: Deprecated; will be come obsolete in near future release

    return 0;
}


int32_t
TCam::onTmAlive(mqs_streamid_t sid M_UU, const char *ep M_UU)
{
    cout << endl << __PRETTY_FUNCTION__ << endl;

    //TODO Implement your ticker master entry functionality here
    //Warning: Deprecated; will be come obsolete in near future release

    return 0;
}


void
TCam::onTimeout()
{

    Mat frame;

    //If there are no images orclients return
    if (_frames->cameraFeed.rows <= 0 || _CamServer.getClientCount() <= 0)
    {
        //cout << "No connections or no images from camera" << endl;
        //cout << "Client count: " << _CamServer.getClientCount() << endl;
        //cout << "cameraFeed rows: " << cameraFeed.rows << endl;
        return;
    }


    Mat baseImg;
    Mat contrastImg;
    Mat productionImg;
    Mat AOIImg;

    //Lock and copy next frame
    osa_mutex_lock(mutex, 0);
    //baseImg.release();
    //contrastImg.release();
    //productionImg.release();
    _frames->cameraFeed.copyTo(baseImg);
    _frames->cameraContrastFeed.copyTo(contrastImg);
    _frames->productViewFeed.copyTo(productionImg);
    //unlock and continue
    osa_mutex_unlock(mutex);

    //Encode the captured fram before send as either png or jpg
    imencode(".jpg", baseImg, liveViewBuffer, compression_params);
    imencode(".jpg", contrastImg, adjustViewBuffer, compression_params);
    imencode(".jpg", productionImg, productionViewBuffer, compression_params);  

    //Encode image to base64 string
    string base64LiveView = base64_encode(liveViewBuffer.data(), liveViewBuffer.size());
    //cout << "Image base64 " << base64 << endl;
    base64LiveView = "{ \"liveData\": \"" + base64LiveView +"\"}";

    
    //Encode image to base64 string
    string base64AdjustView = base64_encode(adjustViewBuffer.data(), adjustViewBuffer.size());
    //cout << "Image base64 " << base64 << endl;
    base64AdjustView = "{ \"contrastData\": \"" + base64AdjustView +"\"}";

    //Encode image to base64 string
    string base64ProductionView = base64_encode(productionViewBuffer.data(), productionViewBuffer.size());
    //cout << "Image base64 " << base64 << endl;
    base64ProductionView = "{ \"productionData\": \"" + base64ProductionView +"\"}";

    //Send the encoded image to all connected clients
    socket2client_t::iterator it;
    auto client = _CamServer.getFirstClient(it);
    for (; client != NULL; client = _CamServer.getNextClient(it))
    {
        //cout << "Sending..." << endl;
        client->sendData(base64LiveView);
        client->sendData(base64AdjustView);
        client->sendData(base64ProductionView);     
    }



}


void TCam::onNewstatehMin(IntegerSlot* slot, int64_t value, Tick t)
{
    std::cout << "hMin changed to: " << value <<std::endl;
    hMin = value;
}
void TCam::onNewstatehMax(IntegerSlot* slot, int64_t value, Tick t)
{
    std::cout << "hMx changed to: " << value <<std::endl;
    hMax = value;
}
void TCam::onNewstatesMin(IntegerSlot* slot, int64_t value, Tick t)
{
    std::cout << "sMin changed to: " << value <<std::endl;
    sMin = value;
}
void TCam::onNewstatesMax(IntegerSlot* slot, int64_t value, Tick t)
{
    std::cout << "sMax changed to: " << value <<std::endl;
    sMax = value;
}
void TCam::onNewstatevMin(IntegerSlot* slot, int64_t value, Tick t)
{
    std::cout << "vMin changed to: " << value <<std::endl;
    vMin = value;
}
void TCam::onNewstatevMax(IntegerSlot* slot, int64_t value, Tick t)
{
    std::cout << "vMax changed to: " << value <<std::endl;
    vMax = value;
}
void TCam::onNewstateproductFlow(StringSlot* slot, string value, Tick t)
{
    std::cout << "Product Flow changed to: " << value <<std::endl;
    productFlow = value;
}

int32_t TCam::getPOIXPos(size_t  POIId)
{
    if(pointsOfInterest->size() > 0 && POIId < pointsOfInterest->size())
    {
        return _config->getPOIXPos(POIId);
    }
    else
    {
        return -1;
    }
}

int32_t TCam::getPOIYPos(size_t POIId)
{
    if(_config->getNPOI() > 0 && POIId < (size_t) _config->getNPOI())
    {
        return _config->getPOIYPos(POIId);
    }
    else
    {
        return -1;
    }
}


void* TCam::sendVideo(void* ctx)
{
    TCam* self = (TCam*) ctx;
    self->fileCapture.open(VIDEOFILEPATH_A);
    int frameCounter = 0;
    //Check if we succeeded
    if(!self->fileCapture.isOpened())
    {
        cout << "Could not open file: " << VIDEOFILEPATH_A << endl;
        return NULL;
        //TODO throw error
    }

   
    do 
    {

        cout << "Sending video" << endl;
        self->fileCapture >> self->frame;
        //Encode the captured fram before send as either png or jpg
        imencode(".jpg", self->frame, self->frameBuffer, self->compression_params);
        
        //Encode image to base64 string
        string base64Frame = self->base64_encode(self->frameBuffer.data(), self->frameBuffer.size());
        //cout << "Image base64 " << base64 << endl;
        base64Frame = "{ \"recording\": \"" + base64Frame +"\"}";

        
        //Send the encoded image to all connected clients
        socket2client_t::iterator it;
        auto client = self->_CamServer.getFirstClient(it);
        for (; client != NULL; client = self->_CamServer.getNextClient(it))
        {
            //cout << "Sending..." << endl;
            client->sendData(base64Frame);
        }

        frameCounter++;
    }while (frameCounter != self->fileCapture.get(CV_CAP_PROP_FRAME_COUNT) - 10  );


    self->fileCapture.release();
    return NULL;
}


//Private methods

void* TCam::TCamLogic(void* ctx)
{
    TCam* self = (TCam*) ctx;
    BaseViewController baseViewController;
    AdjustContrastViewController adjustContrastViewController;
    ProductionViewController productionViewController;
    ProductTracker productTracker;


    while(1)
    {   

        baseViewController.liveCameraView(self->_config, self->_frames, ref(self->mutex));
        adjustContrastViewController.adjustContrast(self->_config, self->_frames,ref(self->mutex));

        //Lock and create next frame
        osa_mutex_lock(self->mutex,0);        
        productTracker.morphImage(self->_frames->thresholdImage);
        self->_frames->areaOfInterestFeed.copyTo( self->_frames->productViewFeed);
        vector<Product> currentProducts = productTracker.findObjects(self->_frames->thresholdImage,
                                                                     self->_frames->productViewFeed,
                                                                     self->_config->getProductFlow());

        if (currentProducts.size()!= 0)
        {
            productTracker.updateTrackedProducts(self->products, &currentProducts, self->_config);
        }
  

        productionViewController.productionView(self->_config, self->_frames, *self->products);

        //unlock mutex and continue
        osa_mutex_unlock(self->mutex);

        productTracker.checkPointsOfInterest(self->_config, self->products);


    }


    return NULL;
}

const std::string TCam::base64_chars = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";


//Encode bytes to base64 string 
std::string TCam::base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len) {
  std::string ret;
  int i = 0;
  int j = 0;
  unsigned char char_array_3[3];
  unsigned char char_array_4[4];

  while (in_len--) {
    char_array_3[i++] = *(bytes_to_encode++);
    if (i == 3) {
      char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
      char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
      char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
      char_array_4[3] = char_array_3[2] & 0x3f;

      for(i = 0; (i <4) ; i++)
        ret += base64_chars[char_array_4[i]];
      i = 0;
    }
  }

  if (i)
  {
    for(j = i; j < 3; j++)
      char_array_3[j] = '\0';

    char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
    char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
    char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
    char_array_4[3] = char_array_3[2] & 0x3f;

    for (j = 0; (j < i + 1); j++)
      ret += base64_chars[char_array_4[j]];

    while((i++ < 3))
      ret += '=';

  }

  return ret;

}






