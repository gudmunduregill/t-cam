/**
 @file BaseViewController.cpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */

#include "BaseViewController.hpp"



BaseViewController::BaseViewController() 
{
    useCamera = USECAMERA; // false;
    useFile = USEFILE;
    useWebcam = USEWEBCAM;
    //Path to video file
    //string videoFilePath = "/home/pluto/video/20180308-01.mp4";
    videoFilePath = "/home/pluto/video/s11.mp4";
    //Device number if using usb camera
    dev = 0; 
    //Frame capture scale factor (larger number smaller image)
    scale = 1;
    frameCounter = 0;
    files = new deque<string>;
    initFileQueue();
    writeVideoJson();

    if(useCamera)
    {
        initializeCamera();
    }
    else if (useFile)
    {
        initializeFileReader();
    }
    else
    {
        initializeWebCam();
    }
  
    codec = CV_FOURCC('t', 'h','e','o'); //ogg
    fps = 5;
    videoSwapper = 0;
}

BaseViewController::~BaseViewController() 
{
    if(!useCamera)
    {
        capture.release(); 
    }
    delete files;
}


//! A Normal member function, updates all points of interest from config file
        /*!
          \param cvImg, image to be rotated
          \param angle, angle to rotate, can be 0, 90, 180, 270 other value as are ignored
        */
void flipImage(Mat &cvImg, int angle);


void BaseViewController::initializeCamera()
{

    // Before using any pylon methods, the pylon runtime must be initialized.
    PylonInitialize();

    try
    {
        // Create an instant camera object with the camera device found first.
        camera = new CInstantCamera(CTlFactory::GetInstance().CreateFirstDevice() );


        // Print the model name of the camera.
        cout << "Using device " << camera->GetDeviceInfo().GetModelName() << endl;

        // The parameter MaxNumBuffer can be used to control the count of buffers
        // allocated for grabbing. The default value of this parameter is 10.
        camera->MaxNumBuffer = 5;

        // Start the grabbing of c_countOfImagesToGrab images.
        // The camera device is parameterized with a default configuration which
        // sets up free-running continuous acquisition.
        camera->Open();
        camera->StartGrabbing(200000000);

        // This smart pointer will receive the grab result data.
        //CGrabResultPtr ptrGrabResult;

        //RGB converter
        //CImageFormatConverter converter;
        converter.OutputPixelFormat=PixelType_BGR8packed;

        //Frame size
        GenApi::CIntegerPtr width(camera->GetNodeMap().GetNode("Width"));
        GenApi::CIntegerPtr height(camera->GetNodeMap().GetNode("Height"));

        //Temp open cv image
//        cvImg(width->GetValue(), height->GetValue(), CV_8UC3);
        return;   
    }
    catch (const GenericException &e)
    {
        // Error handling.
        cerr << "An exception occurred during frame grabbing" << endl
        << e.GetDescription() << endl;
        exit(0);
    }

    return;
}


void BaseViewController::initializeFileReader()
{
   //TODO setup try catch
    cout << "Reading video from file" << endl;
    capture.open(videoFilePath);

    //Check if we succeeded
    if(!capture.isOpened())
    {
        cout << "Could not open file: " << videoFilePath << endl;
        exit(0);
        //TODO throw error
    }
}

void BaseViewController::initializeWebCam()
{
   //TODO setup try catch
    cout << "Using usb camera dev " << dev << endl;
    capture.open(dev);

    //Check if we succeeded
    if(!capture.isOpened())
    {
        cout << "could not open webcam device: " << dev << endl;
        exit(0); 
        //TODO throw error
    }
}


//! A Normal member function, is running as thread from TCAM main method
    /*!
      \param ctx is a _TCAM context.
    */
void BaseViewController::liveCameraView(Config* config,Frames* frames, osa_mutex_t mutex)
{


    if(useCamera)
    {

            //start an infinite loop where one camera frame is copied to cameraFeed matrix of TCam for each loop
//            while(1)
            {

                // Wait for an image and then retrieve it. A timeout of 5000 ms is used.
                camera->RetrieveResult( 5000, ptrGrabResult, TimeoutHandling_ThrowException);

                // Image grabbed successfully?
                if (ptrGrabResult->GrabSucceeded())
                {
                    //Copy image to buffer
                    //const uint8_t *pImageBuffer = (uint8_t *) ptrGrabResult->GetBuffer();
                    //Get buffer size
                    int cols = ptrGrabResult->GetWidth(); 
                    int rows = ptrGrabResult->GetHeight();
                    cv::Size sz(cols/scale,rows/scale);

                    //Create the target
                    CPylonImage target;
                    converter.Convert(target, ptrGrabResult);
    
                    cvImg = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3,(uint8_t*)target.GetBuffer());
                    //Lock and create next frame
                    osa_mutex_lock(mutex,0);              
                    //Copy buffer to TCam structure
                    cvImg.copyTo(frames->cameraFeed);

                    //Make sure AOI is within the frame
                    int AOIwidth = config->getAOIx1() -  config->getAOIx0();
                    int AOIheight = config->getAOIy1() -  config->getAOIy0();
                    int AOIx = config->getAOIx0();
                    int AOIy = config->getAOIy0();
                    if( this->checkAOI(config, cvImg) == false )
                    {                                     
                        AOIwidth = cols;                                   
                        AOIheight = rows;
                        AOIx = 0;
                        AOIy = 0;
                    }

                    cv::Mat AOI(cvImg, cv::Rect(AOIx, AOIy, AOIwidth, AOIheight));

                    AOI.copyTo(frames->areaOfInterestFeed);

                    frames->cameraContrastFeed.copyTo(hsvImg);
                    if (scale != 1)
                    {
                        resize(frames->cameraFeed, frames->cameraFeed, sz);
                    }
                    //unlock mutex and continue
                    osa_mutex_unlock(mutex);

                    if(config->recordVideo())
                    {
                         //First time open the writer
                         if(!videoWriter.isOpened())
                         {
                             string file = "T-CAM_video_" + this->dateTime() +".ogg"; 
                             files->push_front(file);

                             videoWriter.open(VIDEOFILEPATH + file, codec, fps, frames->productViewFeed.size(),true);
                             if (!videoWriter.isOpened()) 
                             {
                                 cerr << "Could not open the output video file for write\n";
                                 return;
                             }
                             videoTimer = time(0);
                         }
                        
                         //Convert camera format to 8 bit
                         //format before saveing
                         Mat image8Bit;
                         Mat image;
                         frames->productViewFeed.copyTo(image);
                         image.convertTo(image8Bit, CV_8U);
                         //Write next frame
                         videoWriter.write(image8Bit);
                         if( difftime( time(0), videoTimer)  > config->getVideoLength())
                         {
                             config->stopRecord();
                             videoTimer = 0;
                             videoWriter.release();
                             //Delete oldest file to ensure disk wont fill up
                             if(files->size() >MAX_N_VIDEOFILES)
                             {
                                 string f = VIDEOFILEPATH + files->front();
                                 if( std::remove( f.c_str()) != 0 )
                                 {
                                     perror( "Error deleting file" );
                                 }
                                 else
                                 {
                                     cout << "File successfully deleted" << endl;;
                                     files->pop_back();
                                 }
                             }
                             writeVideoJson();

                         }

            }


                    /*
                    osa_mutex_lock(mutex,0);
                        imshow("T-CAM", cvImg);
                        imshow("HSV Image", frames->cameraContrastFeed);
                        imshow("Threshold", frames->thresholdImage);
                        imshow("Track", frames->productViewFeed);
                    osa_mutex_unlock(mutex);
                    waitKey(1);
                    */                
                }
                else
                {
                    cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
                }
  
            }
    }
    else
    {
        ///////////////////////////////////////////////////////////////////////
        //    Next lines are to be used with a webcamerea of reading from a file
        ////////////////////////////////////////////////////////////////////////
 
        //start an infinite loop where one camera frame is copied to cameraFeed matrix of TCam for each loop
//        while(1)
        {
            //Capture a frame
            capture >> cvImg;
            //Rotate image accorting to flow direction, only for debuging
            //flipImage(cvImg, 270);

            int cols = (int) capture.get(CV_CAP_PROP_FRAME_WIDTH); 
            int rows = (int) capture.get(CV_CAP_PROP_FRAME_HEIGHT);
            //Lock and capture next frame
            osa_mutex_lock(mutex,0);
                //Copy and resize img
                if (scale != 1)
                {          
                    resize(cvImg, cvImg, cv::Size(cols/scale, rows/scale));
                }
                cvImg.copyTo(frames->cameraFeed);

                //Make sure AOI is within the frame
                int AOIwidth = config->getAOIx1() -  config->getAOIx0();
                int AOIheight = config->getAOIy1() -  config->getAOIy0();
                int AOIx = config->getAOIx0();
                int AOIy = config->getAOIy0();
                if( this->checkAOI(config, cvImg) == false )
                { 
                    AOIwidth = cols;
                    AOIheight = rows;
                    AOIx = 0;
                    AOIy = 0;
                }
               
                cv::Mat AOI(cvImg, cv::Rect(AOIx, AOIy, AOIwidth, AOIheight));

                AOI.copyTo(frames->areaOfInterestFeed);               
                frames->newImage = true;
                frames->newAreaOfInterestImage = true;
            osa_mutex_unlock(mutex);                


            frameCounter++;
            //Restart file if at end
            if(useFile) 
            {
                if( frameCounter == capture.get(CV_CAP_PROP_FRAME_COUNT) )
                {
                    frameCounter = 0;
                    capture.set(CV_CAP_PROP_POS_FRAMES, 0);
                }
            }


            if(config->recordVideo())
            {

                //First time open the writer
                if(!videoWriter.isOpened())
                {
                    string file = "T-CAM_video_" + this->dateTime() +".ogg"; 
                    files->push_front(file);

                    videoWriter.open(VIDEOFILEPATH + file, codec, fps, frames->productViewFeed.size(),true);
                    if (!videoWriter.isOpened()) 
                    {
                        cerr << "Could not open the output video file for write\n";
                        return;
                    }
                    videoTimer = time(0);
                }

		//Convert camera format to 8 bit
		//format before saveing
		Mat image8Bit;
		Mat image;
		frames->productViewFeed.copyTo(image);
		image.convertTo(image8Bit, CV_8U);
		//Write next frame
                videoWriter.write(image8Bit);
                if( difftime( time(0), videoTimer)  > config->getVideoLength())
                {
                    config->stopRecord();
                    videoTimer = 0;
                    videoWriter.release();
                    //Delete oldest file to ensure disk wont fill up
                    if(files->size() >MAX_N_VIDEOFILES)
                    {
                        string f = VIDEOFILEPATH + files->front();
                        if( std::remove( f.c_str()) != 0 )
                        {
                            perror( "Error deleting file" );
                        }
                        else
                        {
                            cout << "File successfully deleted" << endl;;
                            files->pop_back();
                        }
                    }
                    writeVideoJson();
                 }

            }


            //Display image in backend for debug only
            //Lock and capture next frame
            /*
            osa_mutex_lock(mutex,0);
                imshow("T-CAM", cvImg);
                imshow("HSV Image", frames->cameraContrastFeed);
                imshow("Threshold", frames->thresholdImage);
                imshow("Track", frames->productViewFeed);
            osa_mutex_unlock(mutex);
            if(waitKey(1) >= 0 || !capture.isOpened())
            {
                cout << "Error reading from camera. Exiting base view controller" << endl;
                return;
               //break;
            }
            */
        }
    }

    // the camera will be deinitialized automatically in VideoCapture destructor
    return;
}


void flipImage(Mat &cvImg, int angle)
{
    if (angle == 90)
    {
        transpose(cvImg, cvImg);
        flip(cvImg, cvImg,1);   //transpose+flip(1)=CW
    } 
    else if (angle ==180)
    {
        flip(cvImg, cvImg,-1);    //flip(-1)=180          
    } 
    else if (angle == 270) 
    {
        transpose(cvImg, cvImg);
        flip(cvImg, cvImg,0); //transpose+flip(0)=CCW     
    }    
    else if (angle != 0) //if not 0,90,180,270
    {
        //cout << "No action" << endl;
        return;
    }
    return;
}

string BaseViewController::dateTime()
{
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer,sizeof(buffer),"%d-%m-%Y %I:%M:%S",timeinfo);
    std::string dateTimeString(buffer);

    return dateTimeString;
}

void BaseViewController::initFileQueue()
{
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (VIDEOFILEPATH.c_str())) != NULL) 
    {
       /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL)  
        {
            string s = ent->d_name;
            cout << "File Name is: " << s << endl;
            if(s.substr(0, 5) == "T-CAM")
            {
                files->push_front(s);
            }
        }
        closedir (dir);
    } else {
        /* could not open directory */
        perror ("Could not open directory");
        exit(0);
    }


    return;
}

void BaseViewController::writeVideoJson()
{


    string path = VIDEOFILEPATH + "files.json";
    this->fileWriter.open (path.c_str(), std::ofstream::out | std::ofstream::trunc );

    if(files->size() == 0)
    {
        fileWriter << "{ }" << endl;
        fileWriter.close();
        return;
    }


    int noVideos = files->size();
   
    if(fileWriter.is_open())
    {
        fileWriter << "{" << endl;

        for(int i = files->size() - 1; i > 0 ; i-- )
        {
            fileWriter << " \"video" << noVideos - i << "\": \""  << files->at(i) << "\","  << endl ; 
        }

        fileWriter << " \"video" << noVideos << "\": \""  << files->at(0) << "\"}"  << endl ;
    }
    fileWriter.close();
    return;
}



bool BaseViewController::checkAOI(Config* config, Mat cvImg)
{

    if( config->getAOIx0() > cvImg.cols || config->getAOIx0() < 0 )
    {
        //config->setAOIx0((int64_t) 0);
        return false;
    }
    if( config->getAOIy0() > cvImg.rows || config->getAOIy0() < 0 )
    {
        //config->setAOIy0(0);
        return false;
    }
    if( config->getAOIx1() > cvImg.cols || config->getAOIx1() < 0 )
    {
        //config->setAOIx1(cvImg.cols);
        return false;
    }
    if( config->getAOIy1() > cvImg.rows || config->getAOIy1() < 0 )
    {
        //config->setAOIy1(cvImg.rows);
        return false;
    }
    if(config->getAOIx0() >config->getAOIx1())
    {
        //config->setAOIx0(0);
        //config->setAOIx1(cvImg.cols);
        return false;
    }
    if(config->getAOIy0() > config->getAOIy1())
    {
        //config->setAOIy0(0);
        //config->setAOIy1(cvImg.rows);
        return false;
    }

    return true;
}
