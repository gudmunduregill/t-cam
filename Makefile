##################################################
#                                                #
#            Marel Makefile                      #
#                                                #
##################################################

#installation directories for pylon
#PYLON_ROOT ?= /opt/pylon5

PRJ_NAME = TCam

PRJ_TYPE = PROG

MAIN_SRC = TCam.cpp

SRC =  TCam_impl.cpp CamServer.cpp BaseViewController.cpp GlobalVar.cpp AdjustContrastViewController.cpp ProductionViewController.cpp Product.cpp Config.cpp Frames.cpp ProductTracker.cpp PinMessenger.cpp

TEST_SRC = gtest_TCam.cpp gtest_ProductionViewController.cpp gtest_ProductTracker.cpp gtest_PinMessenger.cpp

DOXYGEN = doxygen

TEST_REGR_PRGS =

ADD_CXXFLAGS = -std=c++0x -I/opt/pylon5/include -Wall


ADD_LFLAGS = -L=/opt/pylon5/lib
ADD_LFLAGS +=  -Wl,--enable-new-dtags -Wl,-rpath=/opt/pylon5/lib

TEST_LDFLAGS = -ldigitaliopin++mock -lintegerpin++mock -lgtest_main -lgmock # -lgtest

ADD_LIBS = appbase mdc websocket opencv_features2d opencv_highgui opencv_imgproc opencv_core opencv_ml peermanager 

ADD_LIBS += stringpin++ digitaliopin++ doublepin++ syncpin++ integerpin++ iteminfopin++ measurementpin++

ADD_LIBS += boost_thread

ADD_LIBS += storageconfig


ADD_LIBS +=  -lpylonbase -lpylonutility -lGenApi_gcc_v3_0_Basler_pylon_v5_0 -lGCBase_gcc_v3_0_Basler_pylon_v5_0 


default: debug

ifndef MDEV
    $(error Environment variable MDEV required)
endif

ifndef CPU
    $(error Environment variable CPU <x86/ppc> required)
endif

#doxy 
#    $(DOXYGEN) Doxyfile

#
# include main makefile
#
include $(MDEV)/make/make.main

#eof
