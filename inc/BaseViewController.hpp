#ifndef _BaseViewController_H
#define _BaseViewController_H

#include <stdio.h>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <mutex>
#include <thread>
#include <ctime>  //for video recording

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>

//Include opencv libraries
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <deque>
#include <vector>
#include <fstream>
#include <dirent.h>

#include <TCam.h>
#include <Config.hpp>
#include <Frames.hpp>
#include <GlobalVar.hpp>
#include <PinMessenger.hpp>


//Namespace for using OpenCV
using namespace cv;

//Namespace for using standard library
using namespace std;

// Namespace for using pylon objects.
using namespace Pylon;

//! A frame grabber for T-CAM software module
/*!
    Initializes either a camera communication or a video file writer and grabs 
    one frame when called and generates another subframe using coordinates
    from t-cam.configuration file
 */

class BaseViewController
{
    public:
        CInstantCamera* camera;
        // This smart pointer will receive the grab result data.
        CGrabResultPtr ptrGrabResult;
        //RGB converter
        CImageFormatConverter converter;
        //Video file or webcam
        VideoCapture capture;
        bool useCamera;
        bool useFile;
        bool useWebcam;
        int frameCounter;
        Mat cvImg;
        Mat hsvImg;
        //Path to video file
        string videoFilePath;
        //Device number if using usb camera
        int dev;
        //Frame capture scale factor (larger number smaller image)
        int scale;
        
        VideoWriter videoWriter;
        int codec;
        double fps;
        bool record;
        time_t videoTimer;       
        int videoSwapper;        
        deque<string>* files; 
        BaseViewController();
        ~BaseViewController();

        //! Function that runs a camera and stores each captured
        //! frame into the frames object
        /*!
          \param frames, object containing all of TCAMs image frames
          \param config, object containing all of TCAMs configuration parametrs
          \param mutex, global mutex used when reading/writing common parameters
        */
        void liveCameraView(Config* config, Frames* frames, osa_mutex_t mutex);

    private:
        ofstream fileWriter;
        ifstream fileReader;
        //! Private methood to initialize camera through pylon package
        /*!
          \param
        */
        void initializeCamera();

         //! Private methood to initialize file reader
        /*!
          \param
        */
        void initializeFileReader();

        //! Private methood to initialize web camera
        /*!
          \param
        */
        void initializeWebCam();

        //! A helper function, if AOI coordinates are outside of frame 
        //*!they are lowered  
        /*!
          \param ctx, a TCam context
          \param AOIwidth, width of AOI
          \param AOIheight, height of AOI
          \param cvImg, image that AOI should fit into
        */
        bool checkAOI(Config* config, Mat cvImg);

        //! A simple member function that returns timestamp on the form
        //! 05-05-2018 09:36:49
        /*!
        */
        string dateTime();

        //! A simple member function that reads the index file for video files
        //! and adds to the file queue
        /*!
        */
        void initFileQueue();

        //! A simple member funtion that writes out current video file names
        //! into file
        /*!
        */
        void writeVideoJson();

};

#endif

