/**
 *   @defgroup T-CAM camera tracking algorithm
     @{

     @file TCam.h

     
     
     @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

     <b>
     Copyright (c) 2018 Marel hf
     Proprietary material, all rights reserved, use or disclosure forbidden.
     </b>
 */

#ifndef _TCam_H
#define _TCam_H


#define M_TCam_MODULEID           "TCam"
#ifndef LOGGER_MODULEID
#define LOGGER_MODULEID                     (M_TCam_MODULEID)
#endif

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include "osa.h"
#include <thread>
#include <mutex>


#include <MarelCppApp.h>
#include <CamServer.hpp>
#include <GlobalVar.hpp>
#include <Config.hpp>
#include <Frames.hpp>
#include <BaseViewController.hpp>
#include <AdjustContrastViewController.hpp>
#include <ProductionViewController.hpp>
#include <Product.hpp>
#include <ProductTracker.hpp>

#include <integer_pin.h>
#include <digitalio_pin.h>
#include <string_pin.h>
#include <pluto/pins/integer.hpp>
#include <pluto/pins/string.hpp>
#include <pluto/pins/dio.hpp>
#include <pluto/pins/sync.hpp>
#include <pluto/plog.hpp>
#include <PlutoExcept.h>

#include <ConfigReaderInterface.h>
#include <ConfigWriterInterface.h>
#include <tr1/memory>


//#include <CameraController.hpp>
using namespace Pluto;
using namespace Pluto::Pins;

//!  T-CAM software module, derived from MarelCppApp, a video recognition software used to track a product on a conveyor
/*!
    Implements a tracking algorithm design to communicate and run inside a PLUTO environment. The algorithm includes 
    a frame grabber that grabbs frames from either a camera or a videofile, with the correct configuration it then
    tracks incomming products and notifies PLUTO when they have reached their predefined destination called POI.
    If a product continues past this destination PLUTO is again notified that the piece was not cauged and continued.
*/
class TCam: public MarelCppApp
{
    public:
        //Matrix to store each frame of the webcam feed
        //Live view from camera
        cv::Mat cameraFeed;
        //Threshold image
        cv::Mat cameraContrastFeed;
        cv::Mat thresholdImage;
        //Area of interest, live view image 
        //adjusted in size
        cv::Mat areaOfInterestFeed;
        //Production view, with piece information
        cv::Mat productViewFeed;
        cv::VideoCapture capture;    

        //Configuration parameters
        Config* _config;
        Frames* _frames;

        std::vector<unsigned char> liveViewBuffer;
        std::vector<unsigned char> adjustViewBuffer;
        std::vector<unsigned char> productionViewBuffer;        

        vector<Product>* products;        //Vector of current products
        vector<Point>* pointsOfInterest;

        std::vector<int> compression_params;
        static const std::string base64_chars;
        static const int FRAME_WIDTH = 240;
        static const int FRAME_HEIGHT = 320;
        osa_thread_t baseViewThread;
        osa_thread_t adjustContrastViewThread;
        osa_thread_t productionViewThread;
        osa_thread_t TCamLogicThread;
        osa_thread_t TCamSendVideoThread;
        void *TCamLogicThread_return;
        void *TCamSendVideoThread_return;
        void *baseViewThread_return;
        void *productionViewThread_return;
        void *adjustContrastViewThread_return;
        void* thread_return;
        osa_mutex_t mutex; 
        bool newImage;
        bool newAreaOfInterestImage;
        bool newThresholdImage;
        bool sendVideoFile;
        /*
         * Constructor
      */
        TCam(std::string appName M_UU);

        /*
         * Destructor
         */
        ~TCam();

        /**
         * The timeout function is periodically called by the application
         * loop. The length of the period is defined by the implementer by
         * calling 'setTimeout'
         *
         * @see setTimeout
         */
        void timeout();

        /**
         * Initializes the implementation class
         *
         * @param argc Number of arguments
         * @param argv Array of arguments
         * @return 0 if successful else -1
         */
        int32_t initialize(int32_t argc M_UU, char** argv M_UU);

        /**
         * Deinitializes the implementation class
         */
        void deinitialize();

        /**
         * Called when ticker master deceases
         *
         * DEPRECATED
         */
        int32_t onTmDead(mqs_streamid_t sid M_UU, const char* ep M_UU);

        /**
         * Called when ticker master is up and running
         *
         * DEPRECATED 
         */
        int32_t onTmAlive(mqs_streamid_t sid M_UU, const char* ep M_UU);

  
        /**
         *Called when sending image on CamServer
        */
        void onTimeout();
    
        /**
         *Called to check init status
        */
        bool initFinished() { return initComplete;};

        /**
         *Called to check if Contrasted image is started
        */
        bool cameraContrastFeedIsRunning() { return cameraContrastFeedRunning;};
        
        
        void setCameraContrastFeedRunning(bool val) {cameraContrastFeedRunning = val;};


        //! A Normal member function, returns POI x position as integer -1 
        //  if no points are defined
        /*!
          \param POIId is integer reprecenting an id of a point of interest.
        */
        int32_t getPOIXPos(size_t POIId);

        //! A Normal member function, returns POI y position as integer returns -1 
        //  if no points are defined
        /*!
          \param POIId is integer reprecenting an id of a point of interest.
        */
        int32_t getPOIYPos(size_t POIId);



        //On newstate definitions for HSV threshold values 
        void onNewstatehMin(IntegerSlot* slot, int64_t value, Tick t);
        void onNewstatehMax(IntegerSlot* slot, int64_t value, Tick t);
        void onNewstatesMin(IntegerSlot* slot, int64_t value, Tick t);
        void onNewstatesMax(IntegerSlot* slot, int64_t value, Tick t);
        void onNewstatevMin(IntegerSlot* slot, int64_t value, Tick t);
        void onNewstatevMax(IntegerSlot* slot, int64_t value, Tick t);
        void onNewstateproductFlow(StringSlot* slot, string value, Tick t);
       
    private:
        CamServer _CamServer;
        bool _isStopped;
        eloop_timer_t _timer;
        bool initComplete; 
        bool cameraContrastFeedRunning;
        int numberOfPOI;
        /*
        *Called to convert array to base64 format
        */
        std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len);


         //! A Normal member function, runs the entire TCAM logig as new thread
        /*!
          \param ctx is a pointer to the TCAM context
        */
        static void* TCamLogic(void* ctx); 

         //! A Normal member function, that sends video through CamServer
        /*!
          \param POIId is integer reprecenting an id of a point of interest.
        */
        static void* sendVideo(void* ctx);
 
        StringSignal    _tCamIP;    
        IntegerSlot     _hMin;
        IntegerSlot     _hMax;
        IntegerSlot     _sMin;
        IntegerSlot     _sMax;
        IntegerSlot     _vMin;
        IntegerSlot     _vMax;
        StringSlot      _productFlow;

        Pluto::Plog::LogKey _plog;

        //Contrast threasholds
        int64_t hMin;
        int64_t hMax;
        int64_t sMin;
        int64_t sMax;
        int64_t vMin;
        int64_t vMax;
        string  productFlow;
      
        //Parameters for sendVideo methood
        cv::VideoCapture fileCapture;
        Mat frame;
        vector<unsigned char> frameBuffer;    
};

/**@}*/


#endif /* TCam_H_ */

