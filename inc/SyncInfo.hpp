/**
 @file SyncInfo.hpp

 @author Agust Einarsson, Gudmundur Arnason, Jon Kjartansson <aore@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */
#ifndef _SYNCINFO_H
#define _SYNCINFO_H
//! A struct storing sync message data for products
/*!

*/
struct SyncInfo
{
    int64_t pieceId;
    int64_t weight;
    int64_t length;
    int64_t destination;

    //default constructor
    SyncInfo(int64_t pieceId = 0,  int64_t weight = 0, int64_t destination = 0)
        : pieceId(pieceId), weight(weight), destination(destination)
    {
    }

    inline
    SyncInfo& operator=(const SyncInfo& s) 
    {
        this->pieceId = s.pieceId;
        this->weight = s.weight;
        this->destination = s.destination;
        this->length = s.length;
        return *this;
    }
};

#endif

