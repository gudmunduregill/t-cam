#ifndef _CamServer_H
#define _CamServer_H
#include <pluto/mdc.hpp>
#include <websocket/WebSocketServer.h>
#include <websocket/ClientHandler.h>
#include <vector>
#include <iostream>

//! A simple websocket server, derived from WebSocketServer
/*!
   Implements a simple server, used to serv images from T-CAM back end to the view*s. 
*/
class CamServer: public WebSocketServer
{
    public:
        CamServer();
        virtual ~CamServer();

        ClientHandler* newClient();
        //! Add peer on connect
        /*!
          \param s
          \param str
        */
        bool peerAdded(int s, const std::string& str);
        //! Remove peer on disconnect
        /*!
          \param s
          \param str
        */
        bool peerRemoved(int s, const std::string& str);

    private:
        std::vector<ClientHandler*> _clients;
};

#endif

