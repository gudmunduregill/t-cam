#ifndef _CONFIG_H
#define _CONFIG_H

#include <string>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>

#include <pluto/plog.hpp>
#include <PlutoExcept.h>
#include <ConfigFactory.h>
#include <ConfigWriterFactory.h>
#include <ConfigReaderInterface.h>
#include <ConfigWriterInterface.h>
#include <tr1/memory>
#include <GlobalVar.hpp>

using namespace std;
using namespace cv;

//! A Datastructure that store and handle all of T-CAMs configuration parameters
/*!
    Datastructure, containing parameters and get/set function
*/
class Config {

    public:

        Config();
        ~Config();
        
        //Get functions
        virtual int64_t gethMin();
        virtual int64_t gethMax();
        virtual int64_t getsMin();
        virtual int64_t getsMax();
        virtual int64_t getvMin();
        virtual int64_t getvMax();
        virtual int64_t getAOIx0();
        virtual int64_t getAOIy0();
        virtual int64_t getAOIx1();
        virtual int64_t getAOIy1();
        virtual int64_t getEntranceTolerance();
        virtual int32_t getNPOI();
        virtual string  getProductFlow();
        virtual vector<Point> getPointsOfInterest();
        virtual void updatePointsOfInterest();
        virtual int64_t getOpenBefore();
        virtual int64_t getOpenAfter();
        virtual int64_t getVideoLength();
        virtual int64_t getMaxDistanceAfterPOI();
        virtual bool recordVideo();



        //! A Normal member function, returns POI x position as integer -1 
        //  if no points are defined
        /*!
          \param POIId is integer reprecenting an id of a point of interest.
        */
        virtual int32_t getPOIXPos(size_t POIId);

        //! A Normal member function, returns POI y position as integer returns -1 
        //  if no points are defined
        /*!
          \param POIId is integer reprecenting an id of a point of interest.
        */
        virtual int32_t getPOIYPos(size_t POIId);

        //! A Normal member function, returns POI position depending on  
        //  product flow returns -1 if no points are defined
        /*!
          \param POIId is integer reprecenting an id of a point of interest.
        */
        virtual int64_t getPOIAbsolute(size_t POIId);



        //Set functions
        void sethMin(int64_t val);
        void sethMax(int64_t val);
        void setsMin(int64_t val);
        void setsMax(int64_t val);
        void setvMin(int64_t val);
        void setvMax(int64_t val);
        void setvMax(string val);
        void setAOIx0(int64_t val);
        void setAOIy0(int64_t val);
        void setAOIx1(int64_t val);
        void setAOIy1(int64_t val);
        void setProductFlow(char* productFlow);
        void setEntranceTolerance(int64_t val);
        void setPOI(int IDi, int x, int y); 

        void stopRecord(); 
 
    private:
        //Contrast threasholds
        int64_t hMin;
        int64_t hMax;
        int64_t sMin;
        int64_t sMax;
        int64_t vMin;
        int64_t vMax;
        //Area of interest
        int64_t AOIx0;
        int64_t AOIy0;
        int64_t AOIx1;
        int64_t AOIy1;
        int64_t maxDistanceAfterPOI;
        //Tolerance
        int64_t entranceTolerance;
        string  productFlow;
    
        vector<Point> pointsOfInterest;  
        int nPOI;
        
        //Record parameters
        int64_t videoLength;
        int64_t record;
 
        //Config managers, readers and writers     
        std::tr1::shared_ptr<ConfigReaderInterface> _TCamConfigReaderManager;
        std::tr1::shared_ptr<ConfigWriterInterface> _TCamConfigWriterManager;
        std::tr1::shared_ptr<ConfigReaderInterface> _POIConfigReaderManager;
        std::tr1::shared_ptr<ConfigWriterInterface> _POIConfigWriterManager;

        Pluto::Plog::LogKey _plog;

       
        //! A Normal member function, updates all points of interest from config file
        /*!
          \param 
        */
        void setPointsOfInterest();

    
};


#endif    // _CONFIG_H
