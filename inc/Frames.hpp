#ifndef _FRAMES_H
#define _FRAMES_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>

using namespace cv;

//! A data structur that stores all camera frames used in T-CAM
/*!
    Structure used to share camera frames across T-CAM objects
*/

class Frames {


    public:
        Frames();
        ~Frames();

        //Live view from camera
        Mat cameraFeed;
        //Threshold image
        Mat cameraContrastFeed;
        Mat thresholdImage;
        //Area of interest, live view image 
        //adjusted in size
        Mat areaOfInterestFeed;
        //Production view, with piece information
        Mat productViewFeed;

        //Boolian parameters
        bool newImage;
        bool newAreaOfInterestImage;
        bool newThresholdImage;    

    private:


};




#endif //FRAMES_H
