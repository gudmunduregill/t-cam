#ifndef _AdjustContrastViewController_H
#define _AdjustContrastViewController_H

#include <stdio.h>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <mutex>
#include <thread>

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>

//Include opencv libraries
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>

//#include <TCam.h>
#include <Config.hpp>
#include <Frames.hpp>
#include <GlobalVar.hpp>

//Namespace for using OpenCV
using namespace cv;

//Namespace for using standard library
using namespace std;

// Namespace for using pylon objects.
using namespace Pylon;

//! A Controller class for the Adjust Contrast View, manipulates each camera frame according to HSV values
/*!
 */
class AdjustContrastViewController
{
    public:
        AdjustContrastViewController();
        ~AdjustContrastViewController();
        //! Function that takes raw image and crops out the AreaOfIntres from 
        //! given coordingates (in Config class) and stores it back into frames object
        /*!
          \param config, Config object contining T-Cam parameters
          \param frames, Frames object containing images to be manipulated
          \param mutex, Mutex for global locking while reading/writing images
        */ 
        void adjustContrast(Config* config, Frames* frames, osa_mutex_t mutex);
        
 
    private:
};

#endif

