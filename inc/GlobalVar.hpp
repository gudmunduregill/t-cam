#ifndef _GlobalVar_H
#define _GlobalVar_H
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <osa.h>
#include <vector>
#include <string>

extern cv::Mat BASE_VIEW;
extern const cv::Scalar SCALAR_BLACK; 
extern const cv::Scalar SCALAR_WHITE;
extern const cv::Scalar TCAM_COLOR;
extern const cv::Scalar SCALAR_RED;
extern const cv::Scalar SCALAR_BLUE;
extern int i_out;
extern const int MAX_N_POI;
extern const int FONT;
extern const bool USECAMERA;
extern const bool USEFILE;
extern const bool USEWEBCAM;
extern const std::string VIDEOFILEPATH_A;
extern const std::string VIDEOFILEPATH_B;
extern const std::string VIDEOFILEPATH;
extern const int MAX_N_VIDEOFILES;
#endif

