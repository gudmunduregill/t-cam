#ifndef _PinMessenger_H
#define _PinMessenger_H

#include <string>
#include <queue>          // std::queue

#include <integer_pin.h>
#include <digitalio_pin.h>
#include <string_pin.h>
#include <pluto/pins/integer.hpp>
#include <pluto/pins/string.hpp>
#include <pluto/pins/dio.hpp>
#include <pluto/pins/sync.hpp>
#include <pluto/plog.hpp>
#include <PlutoExcept.h>

#include <GlobalVar.hpp>
#include <SyncInfo.hpp>
#include <Product.hpp>

using namespace Pluto;
using namespace Pluto::Pins;
using namespace std;

//! A messenger that communicates through PLUTO pin structure
/*!
    
*/
class PinMessenger
{
    public:
        PinMessenger();
        virtual ~PinMessenger();
 
        //! A Normal member function, Signals IOmaster to either open or close a gate
        /*!
          \param POIId Id tag of point of interest
          \param action, takes either values "open" or "close"
        */
        virtual void messagePOI(size_t POIId, std::string action, int numberOfPOI);

        //! A Normal member function, updates all points of interest from config file
        /*!
          \param ID, number of pin to be set
          \param val, value to be set 
        */
        virtual void setPOI(int64_t ID, bool val);

        //! A Normal member function, returns the next piece ID from sync message
        /*!
          \param
        */
        //virtual int64_t getPieceId(SyncSlot*, Sync::Item item);
        virtual int64_t getPieceId();

        //! A Normal member function, returns the next piece Weight from sync message
        /*!
          \param
        */
        virtual int64_t getPieceWeight();

        //! A Normal member function, returns the next piece Weight from sync message
        /*!
          \param
        */
        virtual int64_t getPieceLength();


        //! A Normal member function, returns the next piece destination ID
        /*!
          \param
        */
        virtual int64_t getPieceDestination();

        //! A Normal member function, returns the next piece sync info
        /*!
          \param
        */
        virtual SyncInfo getSyncInfo();

        //! A Pluto method that will run when new sync message is sent
        //! Will update SyncInfo queue
        /*!
          \param
        */
        void onNewState(SyncSlot*, Sync::Item item);

        //! A messenger that sends a sync message to the canbus
        /*!
          \param p the product to send messages about
        */
        void sendSyncMessage(Product p);

        //Slots for signaling IOmaster
        DIOSignal _POI00;
        DIOSignal _POI01;
        DIOSignal _POI02;
        DIOSignal _POI03;
        DIOSignal _POI04;
        DIOSignal _POI05;
        DIOSignal _POI06;
        DIOSignal _POI07;
        DIOSignal _POI08;
        DIOSignal _POI09;
        DIOSignal _POI10;
        DIOSignal _POI11;
        //DIOSignal _isRecording;
        SyncSlot _syncMessageSlot;
        SyncSignal _syncMessageSignal;
        Sync::Item _currentSyncItem;
        std::queue<SyncInfo> items;

    private:
};
#endif
