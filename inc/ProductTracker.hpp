#ifndef _PRODUCTTRACKER_H
#define _PRODUCTTRACKER_H

#include <vector>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>

#include <Product.hpp>
#include <Config.hpp>
#include <Frames.hpp>
#include <PinMessenger.hpp>
#include <SyncInfo.hpp>


using namespace cv;


//! An algorithm that takes in camera feed and keeps track of all products in the video
/*!
    Implements a product track that tracks products on a conveyor and stores them in a vector 
    along with their properties. Each time it is called it updates all the products depending on
    next frame.
*/
class ProductTracker {

    public:
        ProductTracker();
        ~ProductTracker();
 
        //! Morphological operation, dilation and erosion, eliminate noice from image
        /*! and combines small details to a solid piece
          \param &thresh is the image to be Morphologically eroded and dilated 
        */
        void morphImage(Mat &threshold);

        //! Tracking method, 
        /*!
          \param threshold is a thresholded image
          \param cameraFeed is the image where product prositions will be marked on 
          \param productFlow is a string indicating product flow
        */
        vector<Product> findObjects(Mat threshold, Mat &cameraFeed, string productFlow);

        //! Update all products positions, add new products
        /*!
          \param products is a vector of tracked products
          \param currentProducts is vector of current products in camera frame
          \param config is an object containing TCAM config parameters
        */
        //void updateTrackedProducts(vector<Product>* trackedProducts, vector<Product> currentImageProducts);
        void updateTrackedProducts(vector<Product>* products, vector<Product>* currentImageProducts, Config* config);

       //!Member function, takes in a product and returns a boolan that states
        //if product is near entrance
        /*!
          \param product a product for the query
          \param config is an object containing TCAM config parameters
        */
        bool isAtEntrance(Product product, Config* config);

        //!Member function, takes in a TCam context, checks all products against
        //their point of interest, if product has reached its POI a signal is sent
        //and product is removed from structure
        /*!
          \param config is an object containing TCAM config parameters
          \param products is a vector of tracked products
        */
        void checkPointsOfInterest(Config* config, vector<Product>* products);

        //Product vector 
        //! A Normal member function that adds a product
        //  If the product's ID is -1 it is added as new product
        //  otherwise it is rejected and returns false
        /*!      
           \param products is a vector containing all products currently tracked
           \param product is the product to be added
        */
        bool addProduct(vector<Product>* products, Product product);

        //! A Normal member function that removes a product
        //  If the product's ID is found product is removed
        //  otherwise it returns false
        /*!      
           \param products is a vector containing all products currently tracked
           \param productID is the ID of product to be removed
       */
        bool removeProductByID(vector<Product>* products, int productID);


        //! A Normal member function that signals PLUTO
        //  with a sync message if a product has not been caught at
        //  its POI
        /*!     
          \param config is an object containing TCAM config parameters
          \param products is a vector of tracked products 
        */
        void handleNotRejectedPieces(Config* config, vector<Product>* products);

    private:
        bool firstImage;
        size_t productID;
        static const int MAX_NUM_PRODUCTS = 50;                
        static const int MIN_PRODUCT_SIZE = 2000; 
        static const int MAX_PRODUCT_SIZE = 300*300;
        PinMessenger* pinMessenger;
        vector<Product>* rejectedProducts;
}; 


#endif //PRODUCTTRACKER_H
