#ifndef _PRODUCT_H
#define _PRODUCT_H

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include <string>

#include <GlobalVar.hpp>

using namespace std;
//! A datastructure descriping a product in the T-CAM context
/*!
    Data structure that stores all information a product needs to have for the 
    tracking algorit 
*/
class Product {

    public:
        Product();
        Product(std::vector<cv::Point> _contour, string productFlow);
        virtual ~Product(void);
        cv::Rect boundingRect;
        std::vector<cv::Point> currentContour;
        bool matchFound;
        bool gateHasOpened;
        bool gateHasClosed;

        //Set functions
        virtual int getXPos();
        virtual int getYPos();
        int getPrevXPos();
        int getPrevYPos();
        int getID();
        size_t getPOI();
        double getArea();
        cv::Moments getMoments();
        cv::Point getCenterPoint();
        void updateProduct(Product p);
        int getLeadingEdge();
        int getTailingEdge();
        cv::Scalar getColor();
        int getWeight();
        int getLength();
        bool isRejected(); 

        //Get functions
        virtual void setXPos(int x);
        virtual void setYPos(int y);
        void setPrevXPos(int y);
        void setPrevYPos(int y);
        void setID(int productID);
        void setPOI(int POI);
        void setMoments(cv::Moments m);
        void setLeadingEdge(cv::Rect rect, string productFlow);
        void setTailingEdge(cv::Rect rect, string productFlow);
        void setColor(cv::Scalar color);
        void setWeight(int val);
        void setLength(int val);
        void setRejectStatus(bool rejected);

    private:
        int ID;
        cv::Point centerPoint;
        size_t POI;
        int xPos;
        int yPos;
        int prevXPos;
        int prevYPos;
        int leadingEdge;
        int tailingEdge;
        double area;
        int weight;
        int length;
        cv::Scalar color;
        cv::Moments moments;
        bool rejected;
};

#endif    // _PRODUCT_H
