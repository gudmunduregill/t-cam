#ifndef _ProductionViewController_H
#define _ProductionViewController_H

#include <stdio.h>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <mutex>
#include <thread>

//Include opencv libraries
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>

//#include <TCam.h>

#include <Frames.hpp>
#include <GlobalVar.hpp>
#include <Config.hpp>
#include <Product.hpp>


//Namespace for using OpenCV
using namespace cv;

//Namespace for using standard library
using namespace std;
//! A controller that generates from tracking data a view for the production 
/*!
   This controller draws on each camera frame all relevant data for each product
   along with tags for each POI in the process
*/
class ProductionViewController
{
    public:

 

        ProductionViewController();
        ~ProductionViewController();

        bool firstImage;
        size_t productID;       
        //! A Normal member function, takes all information available for 
        //! current products at instance when called
        /*!
            \param config, Config object contining T-Cam parameters
            \param frames, Frames object containing images to be manipulated
            \param products, product vector containing products currently being tracked
        */
        void productionView(Config* config, Frames* frames,vector<Product> products);

        //! Draw sync message and product position into view
        /*!
          \param products is a vector contiaining all products currently in the frame
          \param &frame is the frame to be drawn on
          \param color is the font and line color
        */
        void drawObject(vector<Product> products, Mat &frame, Scalar color, int AOIx, int AOIy);

        //! A helper function, Draws points of interest onto view
        /*!
          \param &frame is the frame to be drawn on
        */
        void drawPOI(vector<Point> points, Mat &frame, Scalar color, int AOIx, int AOIy0, int AOIy1);


        //! Update all products positions, add new products
        /*!
          \param config, Config object contining T-Cam parameters
          \param &frame is the frame to be drawn on
        */
        void drawPOI(Config* config, Frames* frames); 
        
        //! A member function, puts text onto a fram inside a filled rectangle 
        /*! Used to draw sync message info on each product
          \param &img is the frame to be drawn on
          \parap p, text insertion point
          \param text, the text
          \param fontSize  Size of font usually between 1 and 10 where 10 is larger
          \param thickness Thickness of font
          \param  color, text color
        */
        void insertTextProduct(Mat &img, Point p, string text, int fontSize, int thickness, Scalar color);

        //! A member function, puts text onto a fram inside a filled rectangle 
        /*! Used to draw the POI onto image
          \param &img is the frame to be drawn on
          \parap p, text insertion point
          \param text, the text
          \param fontSize  Size of font usually between 1 and 10 where 10 is larger
          \param thickness Thickness of font
          \param  color, text color
        */
        void insertText(Mat &img, Point p, string text, int fontSize, int thickness, Scalar color);
               
        //!Calculate distance between two points, returns double
        /*!
          \param p1 is the coordinate for point 1
          \param p2 is the coordinate for point 2
        */
        double distance(Point p1, Point p2);

        

    private:
       std::vector<Mat> numbers; 
};

#endif

